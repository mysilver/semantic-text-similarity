package ml.dataset.feature;

import ml.dataset.feature.value.FeatureValue;

import java.util.ArrayList;
import java.util.List;

public class FeatureExtractor {

    private final List<FeatureCreator> featureCreators;

    public FeatureExtractor(List<FeatureCreator> featureCreators) {
        this.featureCreators = featureCreators;
    }

    public FeatureVector extract(String line) {

        List<FeatureValue> features = new ArrayList<>();
        for (FeatureCreator featureCreator : featureCreators) {
            features.addAll(featureCreator.create(line));
        }
        FeatureVector ret = new FeatureVector();
        ret.setFeatures(features);
        return ret;
    }

}
