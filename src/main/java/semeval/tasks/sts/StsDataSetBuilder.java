package semeval.tasks.sts;

import edu.stanford.nlp.util.Pair;
import ml.dataset.DataSet;
import ml.dataset.FeatureTable;
import ml.dataset.feature.*;
import semeval.tasks.sts.db.SentencePair;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class StsDataSetBuilder {

	public static int NUM_OF_THREADS = 2;

	public static DataSet build(String name, String[] sentenceDatabasePaths, List<FeatureCreator> featureCreators) throws IOException, ExecutionException, InterruptedException {
		return new DataSet(name, toTrainingData(sentenceDatabasePaths,featureCreators), toFeatureAttributes(featureCreators));
	}

	public static DataSet build(String name, String sentenceDatabasePath, List<FeatureCreator> featureCreators) throws IOException, ExecutionException, InterruptedException {
		return new DataSet(name, toTrainingData(new String[] {sentenceDatabasePath},featureCreators), toFeatureAttributes(featureCreators));
	}

	private static FeatureTable toTrainingData(String[] sentenceDatabasePaths, List<FeatureCreator> featureCreators) throws IOException, InterruptedException, ExecutionException {

		FeatureTable ret = new FeatureTable();
		Map<Integer, FeatureVector> featureVectorMap = new HashMap<>();
		FeatureExtractor featureExtractor = new FeatureExtractor(featureCreators);

		ExecutorService executorService = Executors.newFixedThreadPool(NUM_OF_THREADS + 1);
		Producer producer = new Producer(sentenceDatabasePaths);
		producer.counter = new AtomicInteger(0);
		Future<?> submit = executorService.submit(producer);
		List<Consumer> consumers = new ArrayList<>();
		for (int i = 0; i < NUM_OF_THREADS; i++) {
			Consumer consumer = new Consumer(producer, featureVectorMap, featureExtractor);
			consumers.add(consumer);
			executorService.execute(consumer);
		}
		submit.get();
		boolean finished = false;
		while (!finished) {
			finished = true;
			for (Consumer consumer : consumers) {
				finished = finished && !consumer.alive;
			}
		}
		System.out.println("100%");

		synchronized (featureVectorMap) {
			for (Integer integer : featureVectorMap.keySet()) {
				ret.add(featureVectorMap.get(integer));
			}
		}

		executorService.shutdown();
		return ret;
	}

	private static List<FeatureAttribute> toFeatureAttributes(List<FeatureCreator> featureCreators) {

		List<FeatureAttribute> ret = new ArrayList<>();
		for (FeatureCreator featureCreator : featureCreators)
			ret.addAll(featureCreator.attributes());
		ret.add(new FeatureAttribute("Class", FeatureAttribute.AttributeType.NUMERIC));
		return ret;
	}

	private static class Producer implements Runnable {

		private final String[] sentenceDBPaths;
		public AtomicInteger counter;
		public int total;
		public final BlockingQueue<Pair<Integer, String>> queue = new ArrayBlockingQueue<>(NUM_OF_THREADS * 10);
		public boolean finished = false;

		public Producer(String[] sentenceDBPath) {
			this.sentenceDBPaths = sentenceDBPath;
		}

		@Override
		public void run() {
			try {
				long start = System.currentTimeMillis();
				int fileCounter = 0, lineCounter = 0;
				for (String sentenceDBPath : sentenceDBPaths) {

					String inputLine;
					File dbFile = new File(sentenceDBPath);
					BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(dbFile), "UTF8"));
					total = countLines(dbFile);
					
					while ((inputLine = inputReader.readLine()) != null) {
						queue.put(new Pair<>(lineCounter++, inputLine));

						long current = System.currentTimeMillis();
						// float progress = (float) counter.get() * 100 / total;
						float progress = (float) fileCounter * 100 / sentenceDBPaths.length;
						System.out.print("\r" + String.format("%.3f",  progress)+ "%");
						System.out.print(" (Elapsed : "+((current-start)/1000)+" seconds, instances = "+counter.get()+")");

					}
					fileCounter++;
					inputReader.close();
				}
				
				long finish = System.currentTimeMillis();

				System.out.println();
				System.out.println("Producer has finished its job. It took " + ((finish - start)/1000)+" seconds");



			} catch (Exception e) {
				e.printStackTrace();
			}
			finished = true;
		}

		public static int countLines(File aFile) throws IOException {
			LineNumberReader reader = null;
			try {
				reader = new LineNumberReader(new FileReader(aFile));
				while ((reader.readLine()) != null) ;
				return reader.getLineNumber();
			} catch (Exception ex) {
				return -1;
			} finally {
				if (reader != null)
					reader.close();
			}
		}
	}

	private static class Consumer implements Runnable {

		private final Producer producer;
		private final Map<Integer, FeatureVector> featureMap;
		private FeatureExtractor featureExtractor;
		public boolean alive = true;

		private Consumer(Producer producer, Map<Integer, FeatureVector> featureMap, FeatureExtractor featureExtractor) {
			this.producer = producer;
			this.featureMap = featureMap;
			this.featureExtractor = featureExtractor;
		}

		@Override
		public void run() {
			Pair<Integer, String> pair = producer.queue.poll();
			while (!producer.finished || pair != null) {
				if (pair != null) {
					FeatureVector featureVector = featureExtractor.extract(pair.second());
					SentencePair sentencePair = SentencePair.toSentencePair(pair.second());
					featureVector.setClassName(String.valueOf(sentencePair.getClassName()));
					synchronized (featureMap) {
						featureMap.put(pair.first(), featureVector);
					}
					producer.counter.incrementAndGet();
				}
				pair = producer.queue.poll();
			}

			alive = false;
		}
	}
}
