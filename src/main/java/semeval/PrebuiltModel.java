package semeval;

import com.google.gson.Gson;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import de.tudarmstadt.ukp.dkpro.lexsemresource.exception.LexicalSemanticResourceException;
import dkpro.similarity.algorithms.api.SimilarityException;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import nlp.NLPTools;
import semeval.tasks.sts.SemanticTextualSimilarity;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;
import semeval.tasks.sts.feature.creator.NerFC;
import semeval.tasks.sts.feature.creator.NgramFC;
import semeval.tasks.sts.feature.creator.NounVerbMatchingFC;
import semeval.tasks.sts.feature.creator.complex.ComplexFC;
import semeval.tasks.sts.feature.creator.dkpro.LexicalFeatureCreator;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.DiceSimilarity;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.WordNGramsFeatureCreator;
import weka.classifiers.Classifier;
import weka.core.DenseInstance;
import weka.core.SerializationHelper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by may on 1/11/18.
 */
@Path("/")
public class PrebuiltModel {

    static Classifier classifier = null;
    static NLPTools nlpTools = null;
    static List<FeatureCreator> featureCreators = null;

    static {
        String synonymsDirectory = "synonyms/synset.txt";
        try {
            nlpTools = new NLPTools("unigrams", synonymsDirectory, true);
            classifier = (Classifier) SerializationHelper.read("sts_model.weka");
            featureCreators = featureExtractors();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<FeatureCreator> featureExtractors() throws IOException, LexicalSemanticResourceException, SimilarityException {
        List<FeatureCreator> featureCreators = new ArrayList<>();

        featureCreators.add(new ComplexFC(nlpTools));
        featureCreators.add(new LexicalFeatureCreator());

        featureCreators.add(new NgramFC(1, nlpTools, true));
        featureCreators.add(new WordNGramsFeatureCreator(4));
        featureCreators.add(new NounVerbMatchingFC());
        featureCreators.add(new NerFC());
        featureCreators.add(new DiceSimilarity());
        return featureCreators;
    }

    private static Sentence createSentence(String sentence) {
        List<Term> terms_2 = new ArrayList<>();
        List<CoreLabel> sen2_coreLabels = nlpTools.toCoreLabels(sentence);
        for (CoreLabel sen2_coreLabel : sen2_coreLabels) {
            String lemma = sen2_coreLabel.get(CoreAnnotations.LemmaAnnotation.class);
            String pos = sen2_coreLabel.get(CoreAnnotations.PartOfSpeechAnnotation.class);
            Set<String> syns = nlpTools.getSynset(lemma, pos);
            // syns.addAll(babelNet.toSynsets(lemma));
            int sentiment = nlpTools.findSentiment(lemma);
            terms_2.add(new Term(sen2_coreLabel.word(),
                    lemma,
                    pos,
                    syns,
                    sentiment));
        }

        return new Sentence(sentence, terms_2, (short) nlpTools.findSentiment(sentence), nlpTools.nerTokens(sentence));
    }

    private static float similarity(String sentence1, String sentence2) throws Exception {
        SentencePair sentencePair = new SentencePair(createSentence(sentence1), createSentence(sentence2), 0);
        double[] features = new double[18];
        int i = 0;

        for (FeatureCreator fc : featureExtractors()) {
            List<FeatureValue> featureValues = fc.create(sentencePair.toJson());
            for (FeatureValue featureValue : featureValues) {
                features[i++] = Float.parseFloat(featureValue.value());
            }
        }
        return (float) classifier.classifyInstance(new DenseInstance(1, features));
    }

    @POST
    @Path("/similarity")
    @Consumes("application/json")
    public Response service(String json) {
        try {
            Gson gson = new Gson();
            Sentences stringPair = gson.fromJson(json, (Type) Sentences.class);
            float sim = Math.max(similarity(stringPair.sentence1, stringPair.sentence2), 0);
            sim = Math.min(sim, 5);
            return Response.status(200).entity(String.valueOf(sim/5)).build();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    public static void main(String[] args) throws IOException {
        HttpServer httpServer = createHttpServer(5005);
        httpServer.start();

    }

    @SuppressWarnings("restriction")
    private static HttpServer createHttpServer(int portNumber) throws IOException {
        return HttpServerFactory.create(getURI(portNumber));
    }

    private static URI getURI(int portNumber) {
        return UriBuilder.fromUri("http://" + getHostName() + "/").port(portNumber).build();
    }

    private static String getHostName() {
        String hostName = "localhost";
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return hostName;
    }

    static class Sentences {
        String sentence1;
        String sentence2;

        public Sentences() {
        }

        public Sentences(String sentence1, String sentence2) {
            this.sentence1 = sentence1;
            this.sentence2 = sentence2;
        }
    }
}
