package nlp.ner;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import java.util.ArrayList;
import java.util.List;

public class NERTool {

	private final StanfordCoreNLP nlp;

	public NERTool(StanfordCoreNLP nlp) {
		this.nlp = nlp;
	}

	public List<NameEntity> nerTokenize(String test) {

		List tokens = new ArrayList<>();

			// run all Annotators on the passed-in text
			Annotation document = new Annotation(test);
			nlp.annotate(document);

			// these are all the sentences in this document
			// a CoreMap is essentially a Map that uses class objects as keys and has values with
			// custom types
			List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
			StringBuilder sb = new StringBuilder();

			//I don't know why I can't get this code out of the box from StanfordNLP, multi-token entities
			//are far more interesting and useful..
			//TODO make this code simpler..
			for (CoreMap sentence : sentences) {
				// traversing the words in the current sentence, "O" is a sensible default to initialise
				// tokens to since we're not interested in unclassified / unknown things..
				String prevNeToken = "O";
				String currNeToken = "O";
				boolean newToken = true;
				for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
					currNeToken = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
					String word = token.get(CoreAnnotations.TextAnnotation.class);
					// Strip out "O"s completely, makes code below easier to understand
					if (currNeToken.equals("O")) {
						// LOG.debug("Skipping '{}' classified as {}", word, currNeToken);
						if (!prevNeToken.equals("O") && (sb.length() > 0)) {
							handleEntity(prevNeToken, sb, tokens);
							newToken = true;
						}
						continue;
					}

					if (newToken) {
						prevNeToken = currNeToken;
						newToken = false;
						sb.append(word);
						continue;
					}

					if (currNeToken.equals(prevNeToken)) {
						sb.append(" " + word);
					} else {
						// We're done with the current entity - print it out and reset
						// TODO save this token into an appropriate ADT to return for useful processing..
						handleEntity(prevNeToken, sb, tokens);
						newToken = true;
					}
					prevNeToken = currNeToken;
				}
			}
		return tokens;
	}

	private void handleEntity(String inKey, StringBuilder inSb, List inTokens) {
		inTokens.add(new NameEntity(NameEntity.Type.valueOf(inKey), inSb.toString()));
		inSb.setLength(0);
	}
	public static class NameEntity implements Cloneable {

		public enum Type {LOCATION, PERSON, ORGANIZATION, MONEY,
			PERCENT, DATE, TIME, NATIONALITY, TITLE, NUMBER, IDEOLOGY,
			DURATION, MISC, CAUSE_OF_DEATH, ORDINAL, CRIMINAL_CHARGE, SET, RELIGION, COUNTRY}

		private Type name;
		private String value;

		public Type getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public NameEntity(Type name, String value) {
			super();
			this.name = name;
			this.value = value;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			NameEntity that = (NameEntity) o;

			if (name != that.name) return false;
			if (value != null ? !value.equals(that.value) : that.value != null) return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = name != null ? name.hashCode() : 0;
			result = 31 * result + (value != null ? value.hashCode() : 0);
			return result;
		}

		@Override
		public String toString() {
			return "EmbeddedToken{" +
					"name='" + name + '\'' +
					", value='" + value + '\'' +
					'}';
		}

		@Override
		public Object clone() throws CloneNotSupportedException {
			return new NameEntity(this.name, this.value);
		}
	}

}
