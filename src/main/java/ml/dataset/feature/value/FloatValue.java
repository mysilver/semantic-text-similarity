package ml.dataset.feature.value;

public class FloatValue implements FeatureValue {

    private final float value;

    public FloatValue(float value) {
        this.value = value;
    }

    public FloatValue(double value) {
        this.value = (float) value;
    }

    @Override
    public String value() {
        return String.valueOf(value);
    }
}
