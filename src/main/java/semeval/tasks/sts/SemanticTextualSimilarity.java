package semeval.tasks.sts;

import evaluation.WekaEvaluation;
import ml.dataset.DataSet;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.print.formatter.ArffDatasetFormatter;
import ml.dataset.print.formatter.DataSetFormatter;
import nlp.NLPTools;
import semeval.tasks.sts.feature.creator.*;
import semeval.tasks.sts.feature.creator.complex.ComplexFC;
import semeval.tasks.sts.feature.creator.dkpro.LexicalFeatureCreator;
import semeval.tasks.sts.feature.creator.dkpro.StructureFeatureCreator;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.DiceSimilarity;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.OverlapCoefficient;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.WordNGramsFeatureCreator;
import semeval.tasks.sts.feature.creator.dkpro.simulated.structure.POSNGramsFeatureCreator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SemanticTextualSimilarity
{

    public static void main( String[] args ) throws Exception {

        String baseDirectory = SemanticTextualSimilarity.class.getResource("../../../dataset/").getFile() + "/";
		String synonymsDirectory = SemanticTextualSimilarity.class.getResource("../../../synonyms").getFile() + "/synset.txt";

        String[] trainingDatabasePath = new String[] {

				// 2012 test-gold
				baseDirectory + "original/2012/test-gold/Database-MSRpar.txt",
		        baseDirectory + "original/2012/test-gold/Database-MSRvid.txt",
		        baseDirectory + "original/2012/test-gold/Database-SMTeuroparl.txt",
		        baseDirectory + "original/2012/test-gold/Database-surprise.OnWN.txt",
		        baseDirectory + "original/2012/test-gold/Database-surprise.SMTnews.txt",
				// 2012 train
		        baseDirectory + "original/2012/train/Database-MSRpar.txt",
		        baseDirectory + "original/2012/train/Database-MSRvid.txt",
		        baseDirectory + "original/2012/train/Database-SMTeuroparl.txt",

		        // 2013 test gs
		        baseDirectory + "original/2013/test-gs/Database-FNWN.txt",
		        baseDirectory + "original/2013/test-gs/Database-headlines.txt",
		        baseDirectory + "original/2013/test-gs/Database-OnWN.txt",
		        // 2014 test
		        baseDirectory + "original/2014/test/Database-OnWN.txt",
		        baseDirectory + "original/2014/test/Database-tweet-news.txt",
		        baseDirectory + "original/2014/test/Database-deft-news.txt",
		        baseDirectory + "original/2014/test/Database-deft-forum.txt",
		        baseDirectory + "original/2014/test/Database-headlines.txt",
		        baseDirectory + "original/2014/test/Database-images.txt",
		        // 2015
		        baseDirectory + "original/2015/test/Database-answers-forums.txt",
		        baseDirectory + "original/2015/test/Database-answers-students.txt",
		        baseDirectory + "original/2015/test/Database-belief.txt",
		        baseDirectory + "original/2015/test/Database-headlines.txt",
		        baseDirectory + "original/2015/test/Database-images.txt",

				//2016
				baseDirectory + "original/2016/test/Database-answer-answer.txt",
				baseDirectory + "original/2016/test/Database-question-question.txt",
				baseDirectory + "original/2016/test/Database-headlines.txt",
				baseDirectory + "original/2016/test/Database-plagiarism.txt",
				baseDirectory + "original/2016/test/Database-postediting.txt",
				// 2017
				baseDirectory+ "original/2017/test/Database-track5.en-en.txt"
        };
        
        String[] testDatabasePath = new String[] {
		        // 2015
		    /*     baseDirectory + "original/2015/test/Database-answers-forums.txt",
		        baseDirectory + "original/2015/test/Database-answers-students.txt",
		        baseDirectory + "original/2015/test/Database-belief.txt",
		        baseDirectory + "original/2015/test/Database-headlines.txt",
		        baseDirectory + "original/2015/test/Database-images.txt"*/
		        // 2016
//		        baseDirectory + "original/2016/test/Database-answer-answer.txt",
//		        baseDirectory + "original/2016/test/Database-question-question.txt",
//		        baseDirectory + "original/2016/test/Database-headlines.txt",
//		        baseDirectory + "original/2016/test/Database-plagiarism.txt",
//		        baseDirectory + "original/2016/test/Database-postediting.txt"

		       baseDirectory+ "original/2017/test/Database-track5.en-en.txt"
        };

	    NLPTools nlpTools = new NLPTools("/media/may/New Volume/Projects/unigrams", synonymsDirectory);
        List<FeatureCreator> featureCreators = new ArrayList<>();

	    featureCreators.add(new ComplexFC(nlpTools));
	    featureCreators.add(new LexicalFeatureCreator());

	    featureCreators.add(new NgramFC(1, nlpTools, true));
//		featureCreators.add(new WordNGramsFeatureCreator(2));
//		featureCreators.add(new WordNGramsFeatureCreator(3));
		featureCreators.add(new WordNGramsFeatureCreator(4));

//		featureCreators.add(new POSNGramsFeatureCreator(3));
//		featureCreators.add(new POSNGramsFeatureCreator(4));

		featureCreators.add(new NounVerbMatchingFC());
//		featureCreators.add(new SentimentFC());
//		featureCreators.add(new SynVectorFC());

		featureCreators.add(new NerFC());
		featureCreators.add(new DiceSimilarity());
//		featureCreators.add(new OverlapCoefficient());
//		featureCreators.add(new StructureFeatureCreator());
//		featureCreators.add(new DiffMatchFC());

		StsDataSetBuilder.NUM_OF_THREADS = Runtime.getRuntime().availableProcessors();
        System.out.println("Num of cores = " + StsDataSetBuilder.NUM_OF_THREADS);

	    DataSet sts2016 = StsDataSetBuilder.build("STS2016-Training", trainingDatabasePath, featureCreators);
	    writeToArff(sts2016, baseDirectory + "TrainingWekaFile");

	    StringBuilder worseCases = new StringBuilder();
		StringBuilder bestCases = new StringBuilder();
	    WekaEvaluation wekaEvaluation = new WekaEvaluation(baseDirectory + "TrainingWekaFile.arff");

	    int index = 1;
		double numInstances = 0, correlation = 0;
	    for (String testDatabase : testDatabasePath) {

		    sts2016 = StsDataSetBuilder.build("STS2016-Test", new String[]{testDatabase}, featureCreators);
		    writeToArff(sts2016, baseDirectory + "TestWekaFile_" + index);
			System.out.println("\n\n=================== Evaluation Result =======================");
			System.out.println("Test Dataset : " + testDatabase);
			double[] predictions = wekaEvaluation.evaluate(baseDirectory + "TestWekaFile_" + index++ + ".arff");
			numInstances += wekaEvaluation.getNumInstances();
			correlation += wekaEvaluation.getNumInstances() * wekaEvaluation.getCoefficient();
		    System.out.println();
			worseCases.append(WekaEvaluation.explains(true, new String[]{testDatabase}, predictions, 5, featureCreators));
			bestCases.append(WekaEvaluation.explains(false, new String[]{testDatabase}, predictions, 5, featureCreators));

		}

		correlation /= numInstances;
		Thread.sleep(200);
		System.out.println("-------------------------------------------------------------------------");
		System.out.println(" MEAN CORRELATION = 0." + (int)(correlation * 10000) + " for " + (int)numInstances + " instances");
		System.out.println("-------------------------------------------------------------------------");
		Thread.sleep(200);
		System.err.println("Worst cases :");
	    System.err.println(worseCases.toString());
	    Thread.sleep(200);
	    System.out.println("Best cases :");
	    System.out.println(bestCases);
		Thread.sleep(200);
		System.out.println("-------------------------------------------------------------------------");
		System.out.println(" MEAN CORRELATION = 0." + (int)(correlation * 10000) + " for " + (int)numInstances + " instances");
		System.out.println("-------------------------------------------------------------------------");
	}

    private static void writeToArff(DataSet dataSet, String path) {
        DataSetFormatter formatter = new ArffDatasetFormatter();

        try {
            File file = new File(path+".arff");

            file.createNewFile();

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(formatter.format(dataSet));
            bw.close();

            System.err.println("File Path : " + file.getPath());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
