package semeval.tasks.sts;

import ml.dataset.feature.value.FeatureValue;
import nlp.NLPTools;
import org.apache.commons.math.stat.correlation.PearsonsCorrelation;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.feature.creator.complex.ComplexFC;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class UnsupervisedSts {

    public static void main(String[] args) throws Exception {

        String baseDirectory = UnsupervisedSts.class.getResource("../../../dataset/").getFile() + "/";
        String synonymsDirectory = UnsupervisedSts.class.getResource("../../../synonyms").getFile() + "/synset.txt";

        String[] testDatabasePath = new String[]{
                // 2012 test-gold
                 baseDirectory + "original/2012/test-gold/Database-MSRpar.txt",
				baseDirectory + "original/2012/test-gold/Database-MSRvid.txt",
				baseDirectory + "original/2012/test-gold/Database-SMTeuroparl.txt",
				baseDirectory + "original/2012/test-gold/Database-surprise.OnWN.txt",
				baseDirectory + "original/2012/test-gold/Database-surprise.SMTnews.txt",
				// 2012 train
				baseDirectory + "original/2012/train/Database-MSRpar.txt",
				baseDirectory + "original/2012/train/Database-MSRvid.txt",
				baseDirectory + "original/2012/train/Database-SMTeuroparl.txt",

				// 2013 test gs
				baseDirectory + "original/2013/test-gs/Database-FNWN.txt",
				baseDirectory + "original/2013/test-gs/Database-headlines.txt",
				baseDirectory + "original/2013/test-gs/Database-OnWN.txt",
				// 2014 test
				baseDirectory + "original/2014/test/Database-OnWN.txt",
				baseDirectory + "original/2014/test/Database-tweet-news.txt",
				baseDirectory + "original/2014/test/Database-deft-news.txt",
				baseDirectory + "original/2014/test/Database-deft-forum.txt",
				baseDirectory + "original/2014/test/Database-headlines.txt",
				baseDirectory + "original/2014/test/Database-images.txt",
 /*
                // 2015
                baseDirectory + "original/2015/test/Database-answers-forums.txt",
                baseDirectory + "original/2015/test/Database-answers-students.txt",
                baseDirectory + "original/2015/test/Database-belief.txt",
                baseDirectory + "original/2015/test/Database-headlines.txt",
                baseDirectory + "original/2015/test/Database-images.txt"
                // 2016
		        baseDirectory + "original/2016/test/Database-answer-answer.txt",
		        baseDirectory + "original/2016/test/Database-question-question.txt",
		        baseDirectory + "original/2016/test/Database-headlines.txt",
		        baseDirectory + "original/2016/test/Database-plagiarism.txt",
		        baseDirectory + "original/2016/test/Database-postediting.txt"*/

                baseDirectory + "original\\2017\\test\\Database-track5.en-en.txt"
        };

        NLPTools nlpTools = new NLPTools("D:\\Projects\\unigrams", synonymsDirectory);
        ComplexFC complexFC = new ComplexFC(nlpTools);


        double mean = 0;
        int instanceCount = 0;
        PearsonsCorrelation correlation = new PearsonsCorrelation();
        for (String dataset : testDatabasePath) {
            List<Float> actualList = new ArrayList<>();
            List<Float> predictedList = new ArrayList<>();
            Path path = Paths.get(dataset.substring(1, dataset.length()));
            Files.lines(path).forEach(sp -> {

                List<FeatureValue> featureValues = complexFC.create(sp);
                actualList.add(SentencePair.toSentencePair(sp).getClassName());
                predictedList.add(avg(featureValues));
            });
            System.out.println(path.getFileName());
            double c = correlation.correlation(toArray(actualList), toArray(predictedList));
            instanceCount += actualList.size();
            mean += c * actualList.size();
            System.out.println(" Correlation : " + c);
        }

        System.out.println("total ");
        System.out.println(" Correlation : " + mean / instanceCount);


    }

    private static Float avg(List<FeatureValue> featureValues) {
        return (Float.valueOf(featureValues.get(0).value()) + Float.valueOf(featureValues.get(1).value())) / 2;
    }

    private static Float max(List<FeatureValue> featureValues) {
        return Math.max(Float.valueOf(featureValues.get(0).value()), Float.valueOf(featureValues.get(1).value()));
    }

    private static double[] toArray(List<Float> list) {
        double[] ret = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ret[i] = list.get(i);
        }
        return ret;
    }

    private static float min(List<FeatureValue> featureValues) {
        return Math.min(Float.valueOf(featureValues.get(0).value()), Float.valueOf(featureValues.get(1).value()));
    }
}
