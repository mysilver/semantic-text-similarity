package nlp.thesaurus;

import java.util.Set;

/**
 * Created by Mohammad-Ali on 12/05/2016.
 */
public class ThesaurusSense {
    private String sense;
    private String pos;
    private Set<String> synonyms;
    private Set<String> relatedWords;
    private Set<String> antonyms;

    public ThesaurusSense(String sense, String pos, Set<String> synonyms, Set<String> relatedWords, Set<String> antonyms) {
        this.sense = sense;
        this.pos = pos;
        this.synonyms = synonyms;
        this.relatedWords = relatedWords;
        this.antonyms = antonyms;
    }

    public String getSense() {
        return sense;
    }

    public void setSense(String sense) {
        this.sense = sense;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public Set<String> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(Set<String> synonyms) {
        this.synonyms = synonyms;
    }

    public Set<String> getRelatedWords() {
        return relatedWords;
    }

    public void setRelatedWords(Set<String> relatedWords) {
        this.relatedWords = relatedWords;
    }

    public Set<String> getAntonyms() {
        return antonyms;
    }

    public void setAntonyms(Set<String> antonyms) {
        this.antonyms = antonyms;
    }
}
