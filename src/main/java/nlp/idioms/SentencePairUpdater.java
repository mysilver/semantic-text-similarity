package nlp.idioms;

import nlp.thesaurus.Thesaurus;
import nlp.thesaurus.ThesaurusEntity;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.SentencePair;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by may on 4/27/16.
 */
public class SentencePairUpdater {

	public static void main(String[] args) {

		String resourcePath = SentencePairUpdater.class.getClassLoader().getResource("").getFile().replace("target/classes", "src/main/resources/dataset");

		// 2012
		String[] dataset_test_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt",
				"surprise.OnWN.txt",
				"surprise.SMTnews.txt"
		};

		for (String dataset : dataset_test_2012) {
			String tempPath = "original/2012/test-gold/";
			update(resourcePath + tempPath + "Database-" + dataset);
		}

		String[] dataset_train_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt"
		};

		for (String dataset : dataset_train_2012) {
			String tempPath = "original/2012/train/";
			update(resourcePath + tempPath + "Database-" + dataset);
			update(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2013
		String[] dataset_2013 = new String[]{
				"FNWN.txt",
				"headlines.txt",
				"OnWN.txt"
		};

		for (String dataset : dataset_2013) {
			String tempPath = "original/2013/test-gs/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2014
		String[] dataset_2014 = new String[]{
				"deft-forum.txt",
				"deft-news.txt",
				"headlines.txt",
				"images.txt",
				"OnWN.txt",
				"tweet-news.txt"
		};

		for (String dataset : dataset_2014) {
			String tempPath = "original/2014/test/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2015
		String[] dataset_2015 = new String[]{
				"answers-forums.txt",
				"answers-students.txt",
				"belief.txt",
				"headlines.txt",
				"images.txt"
		};

		for (String dataset : dataset_2015) {
			String tempPath = "original/2015/test/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}
		// 2016
		String[] dataset_2016 = new String[]{
				"answer-answer.txt",
				"headlines.txt",
				"plagiarism.txt",
				"postediting.txt",
				"question-question.txt"
		};

		for (String dataset : dataset_2016) {
			String tempPath = "original/2016/test/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}

	}

	static Idioms idioms = new Idioms();
	public static void update(String path) {
		List<SentencePair> ret = new ArrayList<>();

		try {
			String inputLine;
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));

			while ((inputLine = inputReader.readLine()) != null) {
				SentencePair sentencePair = SentencePair.toSentencePair(inputLine);

				Sentence sentence1 = idioms.replacePhrases(sentencePair.getSentence_1());
				Sentence sentence2 = idioms.replacePhrases(sentencePair.getSentence_2());

				ret.add(new SentencePair(sentence1,sentence2,sentencePair.getClassName()));
				if(sentence1.getTerms().size() != sentencePair.getSentence_1().getTerms().size())
					System.out.println(sentence1.getLemmas());
				if(sentence2.getTerms().size() != sentencePair.getSentence_2().getTerms().size())
					System.out.println(sentence2.getLemmas());
			}

			inputReader.close();

			writeFile(path, ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeFile(String path, List<SentencePair> sentencePairs) {
		try {
			File file = new File(path);
			// System.out.println("Writer has started its job, path : " + path);
			file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for (SentencePair sentencePair : sentencePairs) {
				bw.write(sentencePair.toJson() + "\n");
			}
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
