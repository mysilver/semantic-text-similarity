package ml.dataset.feature;

import ml.dataset.feature.value.FeatureValue;

import java.util.List;


public class FeatureVector {

    private String className;
    private List<FeatureValue> features;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<FeatureValue> getFeatures() {
        return features;
    }

    public void setFeatures(List<FeatureValue> features) {
        this.features = features;
    }
}
