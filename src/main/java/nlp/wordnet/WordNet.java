package nlp.wordnet;

import edu.mit.jwi.IRAMDictionary;
import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.data.ILoadPolicy;
import edu.mit.jwi.item.*;
import edu.mit.jwi.morph.WordnetStemmer;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Threadsafe
 */
public class WordNet {

    private IRAMDictionary dictionary;
    private final WordnetStemmer stemmer;

    public WordNet() throws IOException {
        try {
            URL url = getClass().getClassLoader().getResources("dict").nextElement();
            dictionary = new RAMDictionary(url, ILoadPolicy.BACKGROUND_LOAD);
            dictionary.open();
        } catch (Exception e) {
            URL url = new URL("file", "", -1, "dict");
            dictionary = new RAMDictionary(url, ILoadPolicy.BACKGROUND_LOAD);
            dictionary.open();
        }
        stemmer = new WordnetStemmer(this.dictionary);
    }

    public String lemma(String term, POS pos) {
        List<String> stems = stemmer.findStems(term, pos);
        if (stems.size() > 0)
            return stems.get(0);
        return term;
    }

    public Set<String> getSynset(String word, POS pos) {
        Set<String> ret = new HashSet<>();
        IIndexWord indexWord = dictionary.getIndexWord(lemma(word, pos), pos);
        if (indexWord != null) {
            IWordID iWordID = indexWord.getWordIDs().get(0);
            IWord iword = dictionary.getWord(iWordID);
            ISynset synset = iword.getSynset();

            for (IWord iWord : synset.getWords())
                ret.add(iWord.getLemma().toLowerCase());
        }
        ret.add(lemma(word, pos));
        return ret;
    }

}
