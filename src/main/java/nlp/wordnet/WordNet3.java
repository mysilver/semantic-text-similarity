/*
package nlp.wordnet;


import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.*;
import net.didion.jwnl.dictionary.Dictionary;

import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

public class WordNet3 {

    Dictionary dictionary;

    public WordNet3() {
        configureJWordNet();
        dictionary = Dictionary.getInstance();
    }

    public static void main(String[] args)
            throws JWNLException {
        configureJWordNet();
        Dictionary dictionary = Dictionary.getInstance();
        IndexWord word = dictionary.lookupIndexWord(POS.NOUN, "breaks");
        System.out.println("Senses of the word 'wing':");
        Synset[] senses = word.getSenses();
        for (int i=0; i<senses.length; i++) {
            Synset sense = senses[i];
            System.out.println((i+1) + ". " + sense.getGloss());
            Pointer[] holo = sense.getPointers(PointerType.PART_HOLONYM);
            for (int j=0; j<holo.length; j++) {
                Synset synset = (Synset) (holo[j].getTarget());
                Word synsetWord = synset.getWord(0);
                System.out.print("  -part-of-> " + synsetWord.getLemma());
                System.out.println(" = " + synset.getGloss());
            }
        }

    }

    public static void configureJWordNet() {
        // WARNING: This still does not work in Java 5!!!
        try {
            // initialize JWNL (this must be done before JWNL can be used)
            // See the JWordnet documentation for details on the properties file
            JWNL.initialize(new FileInputStream("E:\\Project\\Java\\dkpro\\WordNet_3\\wordnet_properties.xml"));
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }


    public String lemma(String term, POS pos) throws JWNLException {
        IndexWord word = dictionary.lookupIndexWord(pos, term);
        return word.getLemma();
    }

    public Set<String> getSynset(String term, POS pos) throws JWNLException {
        Set<String> ret = new HashSet<>();
        IndexWord word = dictionary.lookupIndexWord(pos, term);
        Synset[] senses = word.getSenses();
        for (Synset sense : senses) {
            ret.add(sense.toString());
        }
        return ret;
    }

    public Set<Synset> getSynsets(String term, POS pos) {
        Set<Synset> ret = new HashSet<>();
        IndexWord word = null;
        try {
            word = dictionary.lookupIndexWord(pos, term);
            if (word == null)
                return ret;
            Synset[] senses = word.getSenses();
            if (senses !=null)
                for (Synset sense : senses) {
                    ret.add(sense);
                }
        }
        catch (JWNLException e) {
            e.printStackTrace();
        }


        return ret;
    }


}
*/
