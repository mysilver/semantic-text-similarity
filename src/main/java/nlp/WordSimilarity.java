package nlp;

import nlp.idioms.Idioms;
import nlp.thesaurus.ThesaurusEntity;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.Set;
import java.util.jar.Pack200;

public class WordSimilarity {

    public enum Type {
        EXACT_MATCH,
        MAJOR_MATCH,
        IN_SYNONYM_SET_L1,
        ACRONYMS,
        PHRASAL_SYNONYM,
        IN_SYNONYM_SET_L2,
        ANTONYMS,
        RELATED_WORDS,
        MAJOR_MATCH_DIFFERENT_DETAILS,
        NONE
    }

    private final NLPTools nlpTools;
    private static Idioms idioms = new Idioms();

    public WordSimilarity(NLPTools nlpTools) {
        this.nlpTools = nlpTools;
    }

    public Type similarity(Term term1, Term term2) {

        if (equals(term1, term2))
            return Type.EXACT_MATCH;

        if (isEqualInLetters(term1, term2))
            return Type.MAJOR_MATCH;

        if (isInWordnetSynset(term1, term2) || isInBabelnetSynset(term1, term2) || isInThesaurus(term1, term2, false))
            return Type.IN_SYNONYM_SET_L1;

        if (isInThesaurusAntonyms(term1, term2))
            return Type.ANTONYMS;

        if (isSubstring(term1, term2))
            return Type.MAJOR_MATCH;

        if (isInThesaurus(term1, term2, true))
            return Type.IN_SYNONYM_SET_L2;

        if (isInThesaurusRelated(term1, term2))
            return Type.RELATED_WORDS;

        return Type.NONE;
    }

    private boolean isInThesaurus(Term term1, Term term2, boolean L2) {
        ThesaurusEntity thesaurusEntity_1 = nlpTools.getThesaurus().getThesaurusEntity(term1.getWord());
        ThesaurusEntity thesaurusEntity_2 = nlpTools.getThesaurus().getThesaurusEntity(term2.getWord());

        // thing

        // few some a couple

        if (thesaurusEntity_1 != null) {
            if (thesaurusEntity_1.hasEqualSenseWith(thesaurusEntity_2, PartsOfSpeech.corenlpToPartsOfSpeech(term2.getPos()), L2))
                return true;
        }

        if (thesaurusEntity_2 != null)
            if(thesaurusEntity_2.hasEqualSenseWith(thesaurusEntity_1, PartsOfSpeech.corenlpToPartsOfSpeech(term1.getPos()), L2))
                return true;

        thesaurusEntity_1 = nlpTools.getThesaurus().getThesaurusEntity(term1.getLemma());
        thesaurusEntity_2 = nlpTools.getThesaurus().getThesaurusEntity(term2.getLemma());

        if (thesaurusEntity_1 != null) {
            if (thesaurusEntity_1.hasEqualSenseWith(thesaurusEntity_2, PartsOfSpeech.corenlpToPartsOfSpeech(term2.getPos())))
                return true;
        }

        if (thesaurusEntity_2 != null)
            if(thesaurusEntity_2.hasEqualSenseWith(thesaurusEntity_1, PartsOfSpeech.corenlpToPartsOfSpeech(term1.getPos())))
                return true;

        return false;

    }

    private boolean isInThesaurusRelated(Term term1, Term term2) {

        ThesaurusEntity thesaurusEntity_1 = nlpTools.getThesaurus().getThesaurusEntity(term1.getWord());
        ThesaurusEntity thesaurusEntity_2 = nlpTools.getThesaurus().getThesaurusEntity(term2.getWord());

        PartsOfSpeech pos1 = PartsOfSpeech.corenlpToPartsOfSpeech(term1.getPos());
        PartsOfSpeech pos2 = PartsOfSpeech.corenlpToPartsOfSpeech(term2.getPos());

        if (pos1 != pos2)
            return false;

        if (thesaurusEntity_1 != null)
            if (thesaurusEntity_1.isRelatedTo(thesaurusEntity_2, pos1))
                return true;

        if (thesaurusEntity_2 != null)
            if(thesaurusEntity_2.isRelatedTo(thesaurusEntity_1, pos1))
                return true;

        thesaurusEntity_1 = nlpTools.getThesaurus().getThesaurusEntity(term1.getLemma().toLowerCase());
        thesaurusEntity_2 = nlpTools.getThesaurus().getThesaurusEntity(term2.getLemma().toLowerCase());

        if (thesaurusEntity_1 != null) {
            if (thesaurusEntity_1.isRelatedTo(thesaurusEntity_2, pos1))
                return true;
        }

        if (thesaurusEntity_2 != null)
            if(thesaurusEntity_2.isRelatedTo(thesaurusEntity_1, pos1))
                return true;

        return false;

    }

    private boolean isInBabelnetSynset(Term term1, Term term2) {

        String lemm_1 = term1.getLemma().toLowerCase();
        String lemm_2 = term2.getLemma().toLowerCase();

        Set<String> term_1_synset = nlpTools.getSynset(lemm_1);
        Set<String> term_2_synset = nlpTools.getSynset(lemm_2);

        if (term_1_synset.contains(lemm_2))
            return true;

        if (term_2_synset.contains(lemm_1))
            return true;

        return false;
    }

    private boolean isInWordnetSynset(Term term1, Term term2) {

        String lemm_1 = term1.getLemma().toLowerCase();
        String lemm_2 = term2.getLemma().toLowerCase();

        if (lemm_1.equals(lemm_2))
            return true;

        if (term1.getSynonyms().contains(lemm_2))
            return true;

        if (term2.getSynonyms().contains(lemm_1))
            return true;

        if (term1.getSynonyms().contains(term2.getWord().toLowerCase()))
            return true;

        if (term2.getSynonyms().contains(term1.getWord().toLowerCase()))
            return true;

        return false;
    }

    private boolean isInThesaurusAntonyms(Term term1, Term term2) {
        ThesaurusEntity thesaurusEntity_1 = nlpTools.getThesaurus().getThesaurusEntity(term1.getWord());
        ThesaurusEntity thesaurusEntity_2 = nlpTools.getThesaurus().getThesaurusEntity(term2.getWord());

        PartsOfSpeech pos1 = PartsOfSpeech.corenlpToPartsOfSpeech(term1.getPos());
        PartsOfSpeech pos2 = PartsOfSpeech.corenlpToPartsOfSpeech(term2.getPos());

        if (pos1 != pos2)
            return false;

        if (thesaurusEntity_1 != null)
            if (thesaurusEntity_1.isAntonymOf(thesaurusEntity_2))
                return true;

        if (thesaurusEntity_2 != null)
            if(thesaurusEntity_2.isAntonymOf(thesaurusEntity_1))
                return true;

        thesaurusEntity_1 = nlpTools.getThesaurus().getThesaurusEntity(term1.getLemma());
        thesaurusEntity_2 = nlpTools.getThesaurus().getThesaurusEntity(term2.getLemma());

        if (thesaurusEntity_1 != null)
            if (thesaurusEntity_1.isAntonymOf(thesaurusEntity_2))
                return true;

        if (thesaurusEntity_2 != null)
            if(thesaurusEntity_2.isAntonymOf(thesaurusEntity_1))
                return true;

        return false;
    }

    private boolean isSubstring(Term term1, Term term2)  {

        String lemm_1 = term1.getLemma().toLowerCase().replace("-","");
        String lemm_2 = term2.getLemma().toLowerCase().replace("-","");
        // pay down == pay
        if ( lemm_1.length() > 2 && lemm_2.length() > 2 && (lemm_1.startsWith(lemm_2) || lemm_1.endsWith(lemm_2) || lemm_2.startsWith(lemm_1 ) || lemm_2.endsWith(lemm_1 )))
            return true;

        return false;
    }

    private boolean isEqualInLetters(Term term1, Term term2) {

        String lemma_1 = term1.getLemma().replaceAll("[^A-Za-z0-9]+", "").toLowerCase();
        String lemma_2 = term2.getLemma().replaceAll("[^A-Za-z0-9]+", "").toLowerCase();


        return  lemma_1.equals(lemma_2)
                && lemma_1.length() > 0
                && lemma_2.length() > 0
                && (lemma_1.length() - term1.getLemma().length() < 3)
                && (term1.getLemma().length() - term2.getLemma().length() < 3);
    }

    private boolean equals(Term term1, Term term2) {
        return term1.getLemma().equalsIgnoreCase(term2.getLemma()) ||  term1.getWord().equalsIgnoreCase(term2.getWord()) ;
    }


    public boolean phrasalSynonym(Term term, SentencePair sentencePair) throws CloneNotSupportedException {

        Sentence inSentence = null;
        Sentence checkSentence;
        for (Term term1 : sentencePair.getSentence_1().getTerms())
            if (term1.equals(term)) {
                inSentence = (Sentence) sentencePair.getSentence_1().clone();
                break;
            }
        if (inSentence == null) {
            inSentence = (Sentence) sentencePair.getSentence_2().clone();
            checkSentence = (Sentence) sentencePair.getSentence_1().clone();
        }
        else
            checkSentence = (Sentence) sentencePair.getSentence_2().clone();

        if (term.getPos().contains("NN") || term.getPos().contains("VB")) {

            Sentence sentence_1 = idioms.replacePhrases(inSentence);
            Sentence sentence_2 = idioms.replacePhrases(checkSentence);

            Term phrasalTerm = null;
            for (Term term1 : sentence_1.getTerms()) {
                if (term1.getLemma().toLowerCase().contains(term.getLemma().toLowerCase()) && (phrasalTerm == null || phrasalTerm.getLemma().length() < term1.getLemma().length()))
                    phrasalTerm = term1;
            }

            for (Term term1 : sentence_2.getTerms()) {
                Type similarity = this.similarity(phrasalTerm, term1);
                if (similarity == Type.EXACT_MATCH || similarity == Type.IN_SYNONYM_SET_L1 || similarity == Type.MAJOR_MATCH)
                    return true;
            }

            for (Term term1 : checkSentence.getTerms()) {
                Type similarity = this.similarity(phrasalTerm, term1);
                if (similarity == Type.EXACT_MATCH || similarity == Type.IN_SYNONYM_SET_L1 || similarity == Type.MAJOR_MATCH)
                    return true;
            }
        }
        return false;
    }
}
