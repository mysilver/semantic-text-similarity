package nlp.idioms;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.Term;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mohammad-Ali on 6/05/2016.
 */
public class CrawlWikiIdioms {

    public static Set<String> idiomFinderFromWikiPage(String url) throws IOException {

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        // Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        Document document = Jsoup.parse(result.toString());
        Elements select = document.select(".mw-category-group a");

        if (select.size()==0)
            select = document.select("li a");

        Set<String> ret = new HashSet<>();
        for (Element element : select) {
            int endIndex = element.text().indexOf('(');
            String text = element.text();
            if (endIndex != -1)
                text = text.substring(0, endIndex).trim();
            // text = text.replace("/"," ");

            ret.add(text);
        }

        return ret;
    }

    public static void main(String[] args) throws IOException {
        String[] urls = new String[] {
                "https://en.wiktionary.org/wiki/Category:English_idioms",
                "https://en.wiktionary.org/wiki/Category:English_set_phrases",
                "https://en.wiktionary.org/wiki/Category:English_rhetorical_questions",
                "https://en.wiktionary.org/wiki/Appendix:English_idioms"
        };

        int counter = 0;
        for (String url : urls) {
            Set<String> strings = idiomFinderFromWikiPage(url);
            for (String string : strings) {
                System.out.println(string);
            }
            counter += strings.size();
        }

        for (char i='A' ; i<='Z' ; i++) {
            Set<String> strings = idiomFinderFromWikiPage("https://en.wiktionary.org/w/index.php?title=Category:English_idioms&from="+i);
            for (String string : strings) {
                System.out.println(string);
            }
            counter += strings.size();
        }


        System.out.println(counter);
    }
}
