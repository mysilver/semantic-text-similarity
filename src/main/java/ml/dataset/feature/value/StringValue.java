package ml.dataset.feature.value;

/**
 * Created by Mohammad-Ali on 19/03/2016.
 */
public class StringValue implements FeatureValue {


    private final String value;

    public StringValue(String value) {
        this.value = value;
    }

    @Override
    public String value() {
        return value;
    }
}
