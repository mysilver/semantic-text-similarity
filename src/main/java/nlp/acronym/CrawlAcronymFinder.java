package nlp.acronym;

import de.tudarmstadt.ukp.wiktionary.api.PartOfSpeech;
import nlp.NLPTools;
import nlp.PartsOfSpeech;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class CrawlAcronymFinder {

    public static List<String> acronymFinder(String acronym, String pos, int page) throws IOException {
        if (!AcronymUtils.isAcronym(acronym, PartsOfSpeech.toWordNetPOS(pos)))
            return new ArrayList<>(0);

        String url = "http://www.acronymfinder.com/" + URLEncoder.encode(acronym + "~" + page, "UTF-8") + ".html";
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

// Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        Document document = Jsoup.parse(result.toString());
        Elements select = document.select(".result-list__body__meaning");

        List<String> ret = new ArrayList<>();
        for (Element element : select) {
            int endIndex = element.text().indexOf('(');
            String text = element.text();
            if (endIndex != -1)
                text = text.substring(0, endIndex).trim();
            // text = text.replace("/"," ");

            ret.add(text);
        }

        return ret;
    }

    public static void main(String[] args) throws IOException {
        acronymFinder("A/C", "NN", 2);
    }

}
