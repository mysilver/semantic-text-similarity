package ml.dataset.print.formatter;

import ml.dataset.DataSet;
import ml.dataset.FeatureTable;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureVector;
import ml.dataset.feature.value.FeatureValue;

import java.util.List;

public class ArffDatasetFormatter implements DataSetFormatter {

    private static char comma = ',';
    private static char opps = '\'';
    private static char ln = '\n';
    private static String ATTRIBUTE = "@attribute ";

    @Override
    public String format(DataSet dataSet) {
        return toString(dataSet.getName())+ln+toString(dataSet.getFeatureAttributes()) + ln + toString(dataSet.getTrainingData());
    }

    public String toString(String name){
        return "@relation " + name;
    }

    protected static String toString(FeatureTable featureTable) {

      //  while (true) {
            StringBuilder stringBuilder= new StringBuilder();
            stringBuilder.append("@data\n");

            try {
                Thread.sleep(1000);
                for (FeatureVector featureVector : featureTable.getFeatureVector()) {
                    for (FeatureValue feature : featureVector.getFeatures()) {
                        stringBuilder.append(feature.value()).append(comma);
                    }
                    stringBuilder.append(featureVector.getClassName()).append(ln);
                }
                return stringBuilder.toString();
            }
            catch (Exception e) {
                e.printStackTrace();
                /*try {
                    Thread.sleep(100);
                }
                catch (InterruptedException e1) {
                    e1.printStackTrace();
                }*/
            }
        //}
        return null;
    }


    protected static String toString(List<FeatureAttribute> featureAttributes) {
        if (featureAttributes == null)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        for (FeatureAttribute attr : featureAttributes) {
            stringBuilder.append(ATTRIBUTE).append(attr.getName());
            if (attr.getPossibleValues()!=null) {
                stringBuilder.append(" { ");
                for (String possibleValue : attr.getPossibleValues()) {
                    stringBuilder.append(opps).append(possibleValue).append(opps).append(comma);
                }
                stringBuilder.deleteCharAt(stringBuilder.length()-1);
                stringBuilder.append('}');
            } else
                stringBuilder.append(" ").append(attr.getAttributeType());
            stringBuilder.append(ln);
        }

        return stringBuilder.toString();
    }
}
