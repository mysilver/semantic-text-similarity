package nlp;

import java.util.ArrayList;
import java.util.List;

/**
 * This class aims to split text to several parts using some delimiters.<br/>
 * The main reason for creating this class was to increase the performance of text chunking.
 */
public class TextSplitter {

    public static String CLAUSE_DELIMITERS = ".·?!؟;:|؛\n\r'»«><)(#@^&*/,~©+=][{}،؛%_—“”-×۔\"";
    public static String SENTENCE_DELIMITERS = ".·?!؟;:|؛\n\r";
    public static String WORD_DELIMITERS = "'»«><)(#@^&*/,~©+=][{}،؛%_—“”-×۔\" \t";
    public static String DOTS_DELIMITERS = ".·";
    public static String SPACE_DELIMITERS = "\t \u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u200B\u202F\u205F\u3000\uFEFF\u2423\u2422\u2420";

    /**
     * Tokenizes the given text using delimiters
     */
    public static List<String> tokenize(String text, String delimiters) {
        return extractNGrams(text, delimiters, 1);
    }

    /**
     * Tokenizes the given text using {@link #SENTENCE_DELIMITERS}, {@link #WORD_DELIMITERS}, and {@link #SPACE_DELIMITERS}
     */
    public static List<String> tokenize(String text) {
        return tokenize(text, SENTENCE_DELIMITERS + WORD_DELIMITERS + SPACE_DELIMITERS);
    }

    /**
     * Extracts n-Grams using delimiters
     */
    public static List<String> extractNGrams(String text, String delimiters, int n) {

        if (text == null)
            return new ArrayList<>(0);

        List<String> ret = new ArrayList<>();
        int splitCount = 0;
        int startPoint = 0;
        boolean preWasDelim = false;
        boolean atBeginning = true;
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < text.length(); i++) {

            char ch = text.charAt(i);
            if (atBeginning && Character.isWhitespace(ch))
                continue;

            if (delimiters.indexOf(ch) != -1) {
                if (preWasDelim)
                    continue;

                preWasDelim = true;
                if (atBeginning)
                    continue;
                splitCount++;
                if (splitCount == 1)
                    startPoint = i;

                if (splitCount == n) {
                    ret.add(sb.toString());
                    sb = new StringBuffer();
                    atBeginning = true;
                    splitCount = 0;
                    i = startPoint;
                    continue;
                }
                sb.append(" ");
            } else {
                sb.append(ch);
                preWasDelim = false;
            }

            if (!preWasDelim)
                atBeginning = false;
        }
        if (text.length() > 0 && delimiters.indexOf(text.charAt(text.length() - 1)) == -1 && (startPoint != 0 || n == 1 && ret.size() == 0)) {
            String s = sb.toString();
            if (s.length() > 0)
                ret.add(s);
        }

        return ret;
    }

    /**
     * Extracts n-Grams from a clause using only {@link #SPACE_DELIMITERS}
     */
    public static List<String> extractNGramsFromClause(String clause, int n) {
        return extractNGrams(clause, SPACE_DELIMITERS, n);
    }

    /**
     * Extracts n-Grams from a text. <br/>
     * Fist of all, this method extract clauses in the text, and then extracts n-Grams for all clauses.
     */
    public static List<String> extractNGrams(String text, int n){
        List<String> ngrams=new ArrayList<>();
        extractClauses(text).forEach(a -> ngrams.addAll(extractNGramsFromClause(a, n)));
        return ngrams;
    }

    private static List<String> extractUnits(String text, String delimiters) {

        if (text == null)
            return new ArrayList<>(0);

        List<String> ret = new ArrayList<>();
        boolean preWasDelim = false;
        boolean begning = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {

            char ch = text.charAt(i);
            if (begning && Character.isWhitespace(ch))
                continue;

            if (begning && delimiters.indexOf(ch) != -1)
                continue;

            if (delimiters.indexOf(ch) != -1 && (isDotSplitter(i, text) || DOTS_DELIMITERS.indexOf(ch) == -1)) {
                if (preWasDelim)
                    continue;

                preWasDelim = true;

                String trim = StringBuilderUtil.trim(sb);
                if (trim.length() != 0)
                    ret.add(trim);

                sb = new StringBuilder();
                begning = true;
            } else {
                sb.append(ch);
                preWasDelim = false;
                begning = false;
            }

        }

        String trim = StringBuilderUtil.trim(sb);
        if (trim.length() != 0)
            ret.add(trim);

        return ret;
    }

    /**
     * Extracts sentences using {@link #SENTENCE_DELIMITERS}
     */
    public static List<String> extractSentences(String text) {
        return extractUnits(text, SENTENCE_DELIMITERS);
    }

    /**
     * Extracts sentences using {@link #CLAUSE_DELIMITERS}
     */
    public static List<String> extractClauses(String text) {
        return extractUnits(text, CLAUSE_DELIMITERS);
    }

    /**
     * This method is used to check if the next character after (i) is {@link #SPACE_DELIMITERS}, {@link #CLAUSE_DELIMITERS}, or EOF
     */
    private static boolean isDotSplitter(int i, String text) {
        return i == text.length() - 1 || Character.isWhitespace(text.charAt(i + 1)) || CLAUSE_DELIMITERS.indexOf(text.charAt(i + 1)) != -1;
    }


}
