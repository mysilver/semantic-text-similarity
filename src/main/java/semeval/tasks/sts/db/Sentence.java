package semeval.tasks.sts.db;

import nlp.ner.NERTool;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Sentence implements Cloneable{

	private final String text;
	private List<Term> terms;
	private final short sentiment;
	private List<NERTool.NameEntity> nameEntities;

	public Sentence(String text, List<Term> terms, short sentiment, List<NERTool.NameEntity> nameEntities) {
		this.text = text;
		this.terms = terms;
		this.sentiment = sentiment;
		this.nameEntities = nameEntities;
	}

	public String getText() {
		return text;
	}

	public List<Term> getTerms() {
		return terms;
	}

	public List<String> getWords() {
		List<String>  ret = new ArrayList<>();
		terms.forEach(term -> ret.add(term.getWord()));
		return ret;
	}

	public List<String> getLemmas() {
		List<String>  ret = new ArrayList<>();
		terms.forEach(term -> ret.add(term.getLemma()));
		return ret;
	}

	public List<String> getPartsOfSpeech() {
		List<String>  ret = new ArrayList<>();
		terms.forEach(term -> ret.add(term.getPos()));
		return ret;
	}

	public String getLemma() {
		return String.join(" ", getLemmas());
	}

	public short getSentiment() {
		return sentiment;
	}

	public Set<String> synonymSet() {
		Set<String> ret = new HashSet<>();
		for (Term term : terms)
			ret.addAll(term.getSynonyms());
		return ret;
	}

	public List<NERTool.NameEntity> getNameEntities() {
		return nameEntities;
	}

	public void addNameEntity(NERTool.NameEntity nameEntity) {
		nameEntities.add(nameEntity);
	}

	public void setNameEntities(List<NERTool.NameEntity> nameEntities) {
		this.nameEntities = nameEntities;
	}

	public void setTerms(List<Term> terms) {
		this.terms = terms;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		List<Term> terms = new ArrayList<>(this.terms.size());
		List<NERTool.NameEntity> nameEntities = new ArrayList<>(this.nameEntities.size());

		for (Term term : this.getTerms())
			terms.add((Term) term.clone());

		for (NERTool.NameEntity nameEntity : this.getNameEntities())
			nameEntities.add((NERTool.NameEntity) nameEntity.clone());

		return new Sentence(this.text, terms,sentiment,nameEntities);
	}
}
