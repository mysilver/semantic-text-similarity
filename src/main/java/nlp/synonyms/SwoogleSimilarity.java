package nlp.synonyms;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by may on 4/26/16.
 */
public class SwoogleSimilarity {

	public static float similarity(String word1, String word2) throws IOException {

		URL url = new URL("http://swoogle.umbc.edu/SimService/GetSimilarity?operation=api&phrase1="+word1.replace(" ","%20")+"&phrase2=" +word2.replace(" ","%20"));
		InputStream is = url.openStream();
		int ptr;
		StringBuffer buffer = new StringBuffer();
		while ((ptr = is.read()) != -1) {
			buffer.append((char)ptr);
		}

		return new Float(buffer.toString());
	}

	public static void main(String[] args) throws IOException {
		float score = similarity("Microwave would be your best bet",
				"Your best bet is research");
		System.out.println(score);
	}

}
