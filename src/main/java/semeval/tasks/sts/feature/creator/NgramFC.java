package semeval.tasks.sts.feature.creator;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import ml.dataset.feature.value.ShortValue;
import nlp.NLPTools;
import nlp.WordSimilarity;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NgramFC implements FeatureCreator {

    private final NLPTools nlpTools;
    private boolean ignoreStopwords;
    private final int ngram;


    public NgramFC(int ngram, NLPTools nlpTools, boolean ignoreStopwords) throws IOException {
        this.ngram = ngram;
        this.nlpTools = nlpTools;
        this.ignoreStopwords = ignoreStopwords;
    }

    @Override
    public List<FeatureValue> create(String line) {

        SentencePair sentencePair = SentencePair.toSentencePair(line);

        List<Term> terms_1 = sentencePair.getSentence_1().getTerms();
        List<Term> terms_2 = sentencePair.getSentence_2().getTerms();

        if (ignoreStopwords) {
            terms_1 = removeStopWords(terms_1);
            terms_2 = removeStopWords(terms_2);
        }

        short ngramMatch = 0;
        for (int i = 0; i < (terms_1.size() - ngram + 1); i++) {
            for (int j = 0; j < (terms_2.size() - ngram + 1); j++) {
                boolean letsCount = true;
                for (int k = 0; k < ngram; k++) {
                    if (!haveTheSameSense(terms_1.get(i + k),terms_2.get(j + k)))
                    {
                        letsCount = false;
                        break;
                    }
                }
                if (letsCount) {
                    ngramMatch++;
                    break;
                }
            }
        }

        List<FeatureValue> ret = new ArrayList<>(1);
        double makhraj = (terms_1.size() - ngram + terms_2.size() - ngram + 2);

        if (makhraj == 0)
            ret.add(new ShortValue(1));
        else
            ret.add(new FloatValue(ngramMatch / makhraj));

        return ret;
    }

    private boolean haveTheSameSense(Term term1, Term term2) {

        WordSimilarity wordSimilarity = new WordSimilarity(nlpTools);

        WordSimilarity.Type similarity = wordSimilarity.similarity(term1, term2);

        if (similarity== WordSimilarity.Type.MAJOR_MATCH || similarity == WordSimilarity.Type.EXACT_MATCH)
            return true;
        return false;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List ret = new ArrayList<>(1);
        ret.add(new FeatureAttribute(ngram + "Gram_" + ignoreStopwords, FeatureAttribute.AttributeType.NUMERIC));
        return ret;
    }



    private List<Term> removeStopWords(List<Term> terms) {
        List<Term> words1WithoutSw = new ArrayList<>();
        for (Term s : terms) {
            if (!nlpTools.isStopWord(s.getLemma()))
                words1WithoutSw.add(s);
        }
        return words1WithoutSw;
    }

}
