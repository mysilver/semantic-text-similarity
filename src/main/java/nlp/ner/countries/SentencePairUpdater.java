package nlp.ner.countries;

import nlp.ner.NERTool;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.io.*;
import java.util.*;

/**
 * Created by may on 4/27/16.
 */
public class SentencePairUpdater {

	public static void main(String[] args) {

		String resourcePath = SentencePairUpdater.class.getClassLoader().getResource("").getFile().replace("target/classes", "src/main/resources/dataset");

		// 2012
		String[] dataset_test_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt",
				"surprise.OnWN.txt",
				"surprise.SMTnews.txt"
		};

		for (String dataset : dataset_test_2012) {
			String tempPath = "original/2012/test-gold/";
			update(resourcePath + tempPath + "Database-" + dataset);
		}

		String[] dataset_train_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt"
		};

		for (String dataset : dataset_train_2012) {
			String tempPath = "original/2012/train/";
			update(resourcePath + tempPath + "Database-" + dataset);
		}

		// 2013
		String[] dataset_2013 = new String[]{
				"FNWN.txt",
				"headlines.txt",
				"OnWN.txt"
		};

		for (String dataset : dataset_2013) {
			String tempPath = "original/2013/test-gs/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2014
		String[] dataset_2014 = new String[]{
				"deft-forum.txt",
				"deft-news.txt",
				"headlines.txt",
				"images.txt",
				"OnWN.txt",
				"tweet-news.txt"
		};

		for (String dataset : dataset_2014) {
			String tempPath = "original/2014/test/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2015
		String[] dataset_2015 = new String[]{
				"answers-forums.txt",
				"answers-students.txt",
				"belief.txt",
				"headlines.txt",
				"images.txt"
		};

		for (String dataset : dataset_2015) {
			String tempPath = "original/2015/test/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}
		// 2016
		String[] dataset_2016 = new String[]{
				"answer-answer.txt",
				"headlines.txt",
				"plagiarism.txt",
				"postediting.txt",
				"question-question.txt"
		};

		for (String dataset : dataset_2016) {
			String tempPath = "original/2016/test/";
			update(resourcePath + tempPath + "Database-" + dataset);

		}

	}

	public static void update(String path) {
		List<SentencePair> ret = new ArrayList<>();
		Countries countries = new Countries();
		try {
			String inputLine;
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));

			while ((inputLine = inputReader.readLine()) != null) {
				SentencePair sentencePair = SentencePair.toSentencePair(inputLine);

				List<NERTool.NameEntity> nameEntities = sentencePair.getSentence_1().getNameEntities();
				List<NERTool.NameEntity> remove = new ArrayList<>();
				for (NERTool.NameEntity nameEntity : nameEntities) {
					if (countries.isCountry(nameEntity.getValue()) && nameEntity.getName() == NERTool.NameEntity.Type.LOCATION)
						remove.add(nameEntity);
				}
				nameEntities.removeAll(remove);
				sentencePair.getSentence_1().setNameEntities(nameEntities);

				for (Term term : sentencePair.getSentence_1().getTerms()) {
					if (countries.isCountry(term.getLemma()))
						sentencePair.getSentence_1().addNameEntity(new NERTool.NameEntity(NERTool.NameEntity.Type.COUNTRY, term.getLemma()));
				}

				nameEntities = sentencePair.getSentence_2().getNameEntities();
				remove = new ArrayList<>();
				for (NERTool.NameEntity nameEntity : nameEntities) {
					if (countries.isCountry(nameEntity.getValue()) && nameEntity.getName() == NERTool.NameEntity.Type.LOCATION)
						remove.add(nameEntity);
				}
				nameEntities.removeAll(remove);
				sentencePair.getSentence_2().setNameEntities(nameEntities);

				for (Term term : sentencePair.getSentence_2().getTerms()) {
					if (countries.isCountry(term.getLemma()))
						sentencePair.getSentence_2().addNameEntity(new NERTool.NameEntity(NERTool.NameEntity.Type.COUNTRY, term.getLemma()));
				}

				ret.add(sentencePair);
			}

			inputReader.close();

			writeFile(path, ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeFile(String path, List<SentencePair> sentencePairs) {
		try {
			File file = new File(path);
			// System.out.println("Writer has started its job, path : " + path);
			file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for (SentencePair sentencePair : sentencePairs) {
				bw.write(sentencePair.toJson() + "\n");
			}
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
