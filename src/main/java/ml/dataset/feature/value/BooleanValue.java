package ml.dataset.feature.value;

/**
 * Created by Mohammad-Ali on 19/03/2016.
 */
public class BooleanValue implements FeatureValue {

    private final boolean value;

    public BooleanValue(boolean value) {
        this.value = value;
    }

    @Override
    public String value() {
        return String.valueOf(value);
    }
}
