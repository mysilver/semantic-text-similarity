package semeval.tasks.sts.feature.creator.complex;

import de.tudarmstadt.ukp.dkpro.lexsemresource.exception.LexicalSemanticResourceException;
import edu.mit.jwi.item.POS;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import nlp.NLPTools;
import nlp.PartsOfSpeech;
import nlp.WordSimilarity;
import nlp.acronym.AcronymUtils;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.ArrayList;
import java.util.List;

public class ComplexFC implements FeatureCreator {

    private final NLPTools nlpTools;
    private final static int EDIT_DISTANCE_THRESHOLD = 1;
    private final static float MAX_SCORE = 120;
    private final EditDistance editDistance;

    public ComplexFC(NLPTools nlpTools) throws LexicalSemanticResourceException {
        this.nlpTools = nlpTools;
        this.editDistance = new EditDistance(nlpTools);
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List<FeatureAttribute> ret = new ArrayList<>();
        for (int i = 1; i < 3; i++) {
            ret.add(new FeatureAttribute(getClass().getSimpleName() + i, FeatureAttribute.AttributeType.NUMERIC));
        }
        return ret;
    }

    @Override
    public List<FeatureValue> create(String line) {

        List<FeatureValue> ret = new ArrayList<>();
        SentencePair sentencePair = SentencePair.toSentencePair(line);

        List<Term> terms_1 = sentencePair.getSentence_1().getTerms();
        List<Term> terms_2 = sentencePair.getSentence_2().getTerms();

        terms_1 = nlpTools.removeStopWords(terms_1, true);
        terms_2 = nlpTools.removeStopWords(terms_2, true);

        int distance = nlpTools.distance(terms_1, terms_2);

        if (distance <= EDIT_DISTANCE_THRESHOLD) {
            FloatValue className = new FloatValue(editDistance.stSimilarity(terms_1, terms_2, distance, sentencePair));
            ret.add(className);
            ret.add(className);
            return ret;
        }

        terms_1 = nlpTools.removeStopWords(terms_1, terms_2.size() <= 5);
        terms_2 = nlpTools.removeStopWords(terms_2, terms_2.size() <= 5);

        if (terms_1.size() == 0 || terms_2.size() == 0) {
            terms_1 = nlpTools.removeStopWords(sentencePair.getSentence_1().getTerms(), true);
            terms_2 = nlpTools.removeStopWords(sentencePair.getSentence_2().getTerms(), true);
        }

        float score_1 = lengthNorm(terms_2.size(), compare(terms_1, terms_2, sentencePair));
        float score_2 = lengthNorm(terms_1.size(), compare(terms_2, terms_1, sentencePair));

        if (!nlpTools.blindSentiment(sentencePair)) {
//            score_1 -= 30 * Math.abs(score_1 + MAX_SCORE) / MAX_SCORE;
//            score_2 -= 30 * Math.abs(score_2 + MAX_SCORE) / MAX_SCORE;
            score_1 -= .05 * (MAX_SCORE - Math.abs(score_1)) ;
            score_2 -= .05 * (MAX_SCORE - Math.abs(score_2)) ;
        }

        ret.add(new FloatValue((score_1 + MAX_SCORE) / (2 * MAX_SCORE / 5)));
        ret.add(new FloatValue((score_2 + MAX_SCORE) / (2 * MAX_SCORE / 5)));
        return ret;
    }

    private float lengthNorm(int length, float score) {

        /*float maxScore = maxScore(terms_1);
        float minScore = minScore(terms_1);
        float v = (score - minScore) / (maxScore - minScore) * 2 * MAX_SCORE - MAX_SCORE;*/

        score = score / (length + 0.5f);

        float lengthNorm = 1.3f - length / 60f;

        if (score < 20)
            return score * .9f * lengthNorm;

        return score * 1.6f * lengthNorm;
    }

    private float compare(List<Term> terms_1, List<Term> terms_2, SentencePair sentencePair) {
        float score = 0;
        for (Term aTerms_2 : terms_2) {

            WordSimilarity.Type similarity = null;

            for (Term aTerms_1 : terms_1) {

                similarity = bestMatch(similarity, nlpTools.similarity(aTerms_2, aTerms_1));

                if (similarity == WordSimilarity.Type.EXACT_MATCH || similarity == WordSimilarity.Type.MAJOR_MATCH || similarity == WordSimilarity.Type.IN_SYNONYM_SET_L1)
                    break;

                if (AcronymUtils.isAcronym(aTerms_2.getWord(), PartsOfSpeech.toWordNetPOS(aTerms_2.getPos())) && !sentencePair.getSentence_1().getText().equals(sentencePair.getSentence_1().getText().toUpperCase()))
                    if (AcronymUtils.appearsInSentence(aTerms_2.getWord(), sentencePair.getSentence_1())) {
                        similarity = WordSimilarity.Type.ACRONYMS;
                        break;
                    }

                if (AcronymUtils.appearsInAcronyms(aTerms_2, sentencePair)) {
                    similarity = WordSimilarity.Type.ACRONYMS;
                    break;
                }
            }

            switch (similarity) {

                case EXACT_MATCH:
                case MAJOR_MATCH:
                case IN_SYNONYM_SET_L1:
                case ACRONYMS:
                    score += coefficient(similarity)
                            * posAward(aTerms_2)
                            * nlpTools.getDfReader().getIDF(aTerms_2.getLemma());
                    break;
                case MAJOR_MATCH_DIFFERENT_DETAILS:
                case IN_SYNONYM_SET_L2:
                case ANTONYMS:
                case RELATED_WORDS:
                case NONE:
                    if (nlpTools.isStopWord(aTerms_2.getLemma()))
                        score += coefficient(null)
                                * posAward(aTerms_2)
                                * nlpTools.getDfReader().getIDF(aTerms_2.getLemma());
                    else
                        score += coefficient(similarity)
                                * posPenalty(aTerms_2)
                                * nlpTools.getDfReader().getIDF(aTerms_2.getLemma());

            }
        }
        return score;
    }

    private float coefficient(WordSimilarity.Type type) {

        if (type == null)
            return 1.5f;

        switch (type) {

            case EXACT_MATCH:
                return 1.3f;
            case MAJOR_MATCH:
                return 1.2f;
            case IN_SYNONYM_SET_L1:
                return 1.3f;
            case MAJOR_MATCH_DIFFERENT_DETAILS:
                return .5f;
            case IN_SYNONYM_SET_L2:
                return .3f;
            case RELATED_WORDS:
                return .3f;
            case ANTONYMS:
                return 2f;
            case ACRONYMS:
                return 1f;
            case NONE:
            default:
                return 1.4f;
        }
    }

    private float minScore(List<Term> terms) {
        final float[] score = {0};
        terms.forEach(term -> score[0] -= 1.1f * posPenalty(term) * nlpTools.getDfReader().getIDF(term.getLemma()));
        return score[0];
    }

    private float maxScore(List<Term> terms) {
        final float[] score = {0};
        terms.forEach(term -> score[0] += 1.3 * posAward(term) * nlpTools.getDfReader().getIDF(term.getLemma()));
        return score[0];
    }

    private float posPenalty(Term term) {
        PartsOfSpeech pos = PartsOfSpeech.corenlpToPartsOfSpeech(term.getPos());
        switch (pos) {
            case NOUN:
                return -14;
            case ADJECTIVE:
                return -2.5f;
            case ADVERB:
                return -.5f;
            case VERB:
                return -8;
            default:
                return -6;
        }
    }

    private float posAward(Term term) {

        POS pos = PartsOfSpeech.toWordNetPOS(term.getPos());
        if (pos != null) {
            switch (pos) {
                case NOUN:
                    return 5f;
                case ADJECTIVE:
                    return 3f;
                case ADVERB:
                    return 1f;
                case VERB:
                    return 6f;
            }
        }
        return 4f;
    }

    private float classScore(float className) {
        return 2 * (MAX_SCORE / 5 * className - MAX_SCORE / 2);
    }

    private WordSimilarity.Type bestMatch(WordSimilarity.Type similarity, WordSimilarity.Type newSimilarity) {
        if (similarity == null)
            return newSimilarity;
        return similarity.ordinal() > newSimilarity.ordinal() ? newSimilarity : similarity;
    }
}
