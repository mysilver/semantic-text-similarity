package nlp.phrase;

import edu.mit.jwi.item.POS;
import nlp.wordnet.WordNet;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by Mohammad-Ali on 8/05/2016.
 */
public class CrawlPhrasalVerbs {

    public static Set<String> phrasalVerbFinder() throws IOException {

        String url = "http://www.easypacelearning.com/all-lessons/grammar/1219-phrasal-verbs-list-meanings-and-examples";
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

// Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        Document document = Jsoup.parse(result.toString());
        Elements select = document.select("strong");

        if (select.size()==0)
            select = document.select("li a");

        Set<String> ret = new HashSet<>();
        for (Element element : select) {
            String text = element.text();

            // text = text.replace("/"," ");

            ret.add(text);
        }

        return ret;
    }

    public static Set<String> getInseparablePhrasalVerbs() {
        Set<String> phrasalVerbs = new HashSet<>();
        String file = PhrasalVerbs.class.getResource("/phrasal-verbs-inseparable.txt").getPath();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine())!=null) {
                phrasalVerbs.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return phrasalVerbs;
    }

    public static void main(String[] args) throws IOException {
        Set<String> strings = phrasalVerbFinder();
       // BabelNet babelNet = new BabelNet();
        WordNet wordNet = new WordNet();
        Set<String> inseparablePhrasalVerbs = getInseparablePhrasalVerbs();
        strings.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                try {
                   // Set<String> strings1 = babelNet.toSynsets(s.toLowerCase());
                    Set<String> strings2 = wordNet.getSynset(s.toLowerCase(), POS.VERB);
                   // strings1.addAll(strings2);
                  /*  if (!inseparablePhrasalVerbs.contains(s))
                        System.out.println(s+"\t"+"false\t"+String.join(",", strings1));*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        for (String inseparablePhrasalVerb : inseparablePhrasalVerbs) {
            //Set<String> strings1 = babelNet.toSynsets(inseparablePhrasalVerb.toLowerCase());
            Set<String> strings2 = wordNet.getSynset(inseparablePhrasalVerb.toLowerCase(), POS.VERB);
            //strings2.addAll(strings2);
            System.out.println(inseparablePhrasalVerb+"\t"+"true\t"+String.join(",", strings2));
        }
        System.out.println(strings.size());
    }
}
