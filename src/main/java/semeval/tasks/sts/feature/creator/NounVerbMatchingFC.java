package semeval.tasks.sts.feature.creator;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.PossibleValues;
import ml.dataset.feature.value.BooleanValue;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.ArrayList;
import java.util.List;


public class NounVerbMatchingFC implements FeatureCreator {

	private static final String NOUN = "NN";
	private static final String VERB = "VB";
	private static final String ADJ = "JJ";


	@Override
	public List<FeatureValue> create(String line) {
		SentencePair sentencePair = SentencePair.toSentencePair(line);

		List<Term> terms_1 = sentencePair.getSentence_1().getTerms();
		List<Term> terms_2 = sentencePair.getSentence_2().getTerms();

//		List<String> nounsPOS = new ArrayList<>();
		List<String> nouns = new ArrayList<>();
//		List<String> verbsPOS = new ArrayList<>();
		List<String> verbs = new ArrayList<>();
//		List<String> adjsPOS = new ArrayList<>();
//		List<String> adjs = new ArrayList<>();


		for (Term te : terms_1) {
			String POS = te.getPos();
			if (POS.contains(NOUN)) {
//				nounsPOS.add(POS);
				nouns.add(te.getLemma());
			}

			if (POS.contains(VERB)) {
//				verbsPOS.add(POS);
				verbs.add(te.getLemma());
			}
/*
			if (POS.contains(ADJ)) {
				adjsPOS.add(POS);
				adjs.add(te.getLemma());
			}*/

		}

		float numOfNouns = nouns.size();
		float numOfVerbs = verbs.size();
		/*float numOfAdjc = adjs.size();*/

		for (Term term : terms_2) {
			String POS = term.getPos();

			if (POS.contains(NOUN)) {
//				nounsPOS.remove(POS);
				nouns.removeAll(term.getSynonyms());
			}

			if (POS.contains(VERB)) {
//				verbsPOS.remove(POS);
				verbs.removeAll(term.getSynonyms());
			}

//			if (POS.contains(ADJ)) {
//				adjsPOS.remove(POS);
//				adjs.removeAll(term.getSynonyms());
//			}
		}

		List<FeatureValue> ret = new ArrayList<>(2);

		double lengthNorm = Math.log((sentencePair.getSentence_1().getTerms().size() + sentencePair.getSentence_1().getTerms().size()) / 2);

	//	ret.add(new BooleanValue(nounsPOS.size()==0));
		if (numOfNouns!=0) {
			float v = (numOfNouns - nouns.size()) / numOfNouns;
			ret.add(new FloatValue(v * v * lengthNorm));
		}
		else
			ret.add(new FloatValue(lengthNorm));

	//	ret.add(new BooleanValue(verbsPOS.size()==0));
		/*if (numOfVerbs!=0) {
			double v = (numOfVerbs - verbs.size()) / numOfVerbs;
			ret.add(new FloatValue(v * v * lengthNorm));
		}
		else
			ret.add(new FloatValue(lengthNorm));*/

		/*ret.add(new BooleanValue(adjsPOS.size()==0));
		if (numOfAdjc!=0) {
			double v  = (numOfAdjc - adjs.size()) / numOfAdjc;
			ret.add(new FloatValue(v * v * lengthNorm));
		}
		else
			ret.add(new FloatValue(lengthNorm));*/

		return ret;

	}

	@Override
	public List<FeatureAttribute> attributes() {
		List<FeatureAttribute> ret = new ArrayList<>(1);
		//ret.add(new FeatureAttribute("NounMatch", PossibleValues.booleanValues()));
		ret.add(new FeatureAttribute("NounMatchPercentage", FeatureAttribute.AttributeType.NUMERIC));
		//ret.add(new FeatureAttribute("VerbMatch", PossibleValues.booleanValues()));
//		ret.add(new FeatureAttribute("VerbMatchPercentage", FeatureAttribute.AttributeType.NUMERIC));
//		ret.add(new FeatureAttribute("AdjMatch", PossibleValues.booleanValues()));
//		ret.add(new FeatureAttribute("AdjectiveMatchPercentage", FeatureAttribute.AttributeType.NUMERIC));
		return ret;
	}
}
