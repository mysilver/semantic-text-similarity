package semeval.tasks.sts.feature.creator.complex;

import edu.mit.jwi.item.POS;
import nlp.NLPTools;
import nlp.PartsOfSpeech;
import nlp.ner.NERTool;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.List;

public class EditDistance {

    private final NLPTools nlpTools;

    public EditDistance(NLPTools nlpTools) {
        this.nlpTools = nlpTools;
    }

    public float stSimilarity(List<Term> terms_1, List<Term> terms_2, int distance, SentencePair sentencePair) {

        terms_1 = nlpTools.removeTheSameWords(terms_1, terms_2);
        terms_2 = nlpTools.removeTheSameWords(terms_2, sentencePair.getSentence_1().getTerms());

        switch (distance) {
            case 0:
                return 5;
            case 1:
                return compareEditDistance1(terms_1.size() > 0 ? terms_1.get(0) : null, terms_2.size() > 0 ? terms_2.get(0) : null, sentencePair);
            default:
                return compareEditDistance(terms_1, terms_2, sentencePair);
        }
    }

    private float compareEditDistance(List<Term> terms_1, List<Term> terms_2, SentencePair sentencePair) {

        terms_1 = nlpTools.removeStopWords(terms_1, false);
        terms_2 = nlpTools.removeStopWords(terms_2, false);

        if (terms_1.size() == 0 && terms_2.size() == 0)
            return 5;

       /* if (terms_1.size() < 2 && terms_2.size() < 2)
            return compareEditDistance1(terms_1.size() > 0 ? terms_1.get(0) : null, terms_2.size() > 0 ? terms_2.get(0) : null, sentencePair);
*//*

        terms_1 = nlpTools.removeStopWords(terms_1, false);
        terms_2 = nlpTools.removeStopWords(terms_2, false);

        if (terms_1.size() == 0 && terms_2.size() == 0)
            return classScore(5);
*/

        throw new UnsupportedOperationException();
       /* float sum = 0;
        for (Term term1 : terms_1) {
            float max = Float.MIN_VALUE;

            for (Term term2 : terms_2) {
                float score = compareEditDistance1(term1, term2, sentencePair);
                if (max < score)
                    max = score;
            }
            sum += max;
        }
        float v = sum / terms_1.size();
        return v;*/
    }

    private float compareEditDistance1(Term term1, Term term2, SentencePair sentencePair) {

        if (term1 == null && term2 == null)
            return 5;

        if (term1 == null)
            return missingTermScore(term2, sentencePair);

        if (term2 == null)
            return missingTermScore(term1, sentencePair);

        switch (nlpTools.similarity(term1, term2)) {
            case EXACT_MATCH:
            case MAJOR_MATCH:
                return 5;
            case IN_SYNONYM_SET_L1:
                return 5;
            case IN_SYNONYM_SET_L2:
                return 4;
            case RELATED_WORDS:
                return 2.5f;
            case ANTONYMS:
            case MAJOR_MATCH_DIFFERENT_DETAILS:
                return 1;
            case NONE:
                break;
        }

        return notTheSameSenseScore(term1, term2, sentencePair);
    }

    private float notTheSameSenseScore(Term term1, Term term2, SentencePair sentencePair) {

        POS pos = PartsOfSpeech.toWordNetPOS(term1.getPos());
        POS pos2 = PartsOfSpeech.toWordNetPOS(term2.getPos());

        NERTool.NameEntity nameEntity1 = nlpTools.getNameEntity(term1, sentencePair.getSentence_1());
        NERTool.NameEntity nameEntity2 = nlpTools.getNameEntity(term2, sentencePair.getSentence_2());

        if (nameEntity2 != null && nameEntity1 != null)
            return 2;

        // few some
        if (pos == pos2) {
            if (pos != null) {
                switch (pos) {
                    case NOUN:
                    case VERB:
                    case ADJECTIVE:
                        return 2;
                    case ADVERB:
                        return 5;
                }
            }
            return 5;
        }

        if (nlpTools.isStopWord(term1.getLemma()) && nlpTools.isStopWord(term2.getLemma()))
            return 5;
        else if (term1.getSentiment() == term2.getSentiment())
            return 3;
        else
            return 1;
    }

    private float missingTermScore(Term term, SentencePair sentencePair) {
        PartsOfSpeech pos = PartsOfSpeech.corenlpToPartsOfSpeech(term.getPos());

        switch (term.getLemma().toLowerCase()) {
            case "not":
            case "never":
            case "neither":
            case "no":
            case "none":
            case "nor":
                return 1;
        }

        if (nlpTools.isStopWord(term.getLemma()))
            return 5;

        switch (pos) {

            case NOUN:
            case NOUN_PHRASE:
                return 3.5f;
            case VERB:
            case PHRASAL_VERB:
                return 5f;
            case ADJECTIVE:
                return 4f;
            case ADVERB:
            case NONE:
                return 5;
        }
        return 4f;
    }

}
