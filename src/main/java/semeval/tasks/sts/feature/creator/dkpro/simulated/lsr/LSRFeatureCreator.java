package semeval.tasks.sts.feature.creator.dkpro.simulated.lsr;

import de.tudarmstadt.ukp.dkpro.lexsemresource.Entity;
import de.tudarmstadt.ukp.dkpro.lexsemresource.LexicalSemanticResource;
import de.tudarmstadt.ukp.dkpro.lexsemresource.exception.LexicalSemanticResourceException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiRelatednessException;
import dkpro.similarity.algorithms.api.SimilarityException;
import dkpro.similarity.algorithms.lsr.aggregate.MCS06AggregateComparator;
import dkpro.similarity.algorithms.lsr.gloss.GlossOverlapComparator;
import dkpro.similarity.algorithms.lsr.path.*;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LSRFeatureCreator implements FeatureCreator {

	private final LexicalSemanticResource resource;
	private GlossOverlapComparator glossOverlapComparator;
	private MCS06AggregateComparator mcs06AggregateComparator;
	private JiangConrathComparator jiangConrathComparator;
	private LeacockChodorowComparator leacockChodorowComparator;
	private LinComparator linComparator;
	private PathBasedComparator pathBasedComparator;
	private PathLengthComparator pathLengthComparator;
	private ResnikComparator resnikComparator;
	private WuPalmerComparator wuPalmerComparator;


	public LSRFeatureCreator(LexicalSemanticResource resource) throws LexicalSemanticResourceException, SimilarityException, WikiRelatednessException, WikiApiException {
		this.resource = resource;
		glossOverlapComparator = new GlossOverlapComparator(resource,true);
//		mcs06AggregateComparator = new MCS06AggregateComparator();
		/*jiangConrathComparator = new JiangConrathComparator(resource);
		leacockChodorowComparator = new LeacockChodorowComparator(resource);
		linComparator = new LinComparator(resource);
		pathLengthComparator = new PathLengthComparator(resource);
		resnikComparator = new ResnikComparator(resource);
		wuPalmerComparator = new WuPalmerComparator(resource);*/
	}

	@Override
	public List<FeatureValue> create(String line) {
		SentencePair sentencePair = SentencePair.toSentencePair(line);
		List<FeatureValue> ret = new ArrayList<>(1);
		Set<Entity> entities_1 = new HashSet<>();
		Set<Entity> entities_2 = new HashSet<>();
		try {

			for (Term term : sentencePair.getSentence_1().getTerms()) {
				entities_1.addAll(resource.getEntity(term.getLemma(), Entity.PoS.unk));
			}

			for (Term term : sentencePair.getSentence_2().getTerms()) {
				entities_2.addAll(resource.getEntity(term.getLemma(), Entity.PoS.unk));
			}

			ret.add(new FloatValue(glossOverlapComparator.getSimilarity(entities_1, entities_2)));
//			ret.add(new FloatValue(jiangConrathComparator.getSimilarity(entities_1, entities_2)));
//			ret.add(new FloatValue(leacockChodorowComparator.getSimilarity(entities_1, entities_2)));
//			ret.add(new FloatValue(linComparator.getSimilarity(entities_1, entities_2)));
//			ret.add(new FloatValue(pathLengthComparator.getSimilarity(entities_1, entities_2)));
//			ret.add(new FloatValue(resnikComparator.getSimilarity(entities_1, entities_2)));
//			ret.add(new FloatValue(wuPalmerComparator.getSimilarity(entities_1, entities_2)));

		} catch (Exception e) {
			e.printStackTrace();
			ret.add(new FloatValue(0));

		}

		return ret;
	}

	@Override
	public List<FeatureAttribute> attributes() {
		List<FeatureAttribute> ret = new ArrayList<>();
		ret.add(new FeatureAttribute("GlossOverlap"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		//ret.add(new FeatureAttribute("MCS06Aggregate"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		/*ret.add(new FeatureAttribute("JiangConrath"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		ret.add(new FeatureAttribute("LeacockChodorow"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		ret.add(new FeatureAttribute("Lin"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		ret.add(new FeatureAttribute("PathLength"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		ret.add(new FeatureAttribute("Resnik"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));
		ret.add(new FeatureAttribute("WuPalmer"+ resource.getResourceName(), FeatureAttribute.AttributeType.NUMERIC));*/
		return ret;
	}
}
