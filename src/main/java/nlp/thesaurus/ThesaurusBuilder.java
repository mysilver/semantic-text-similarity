package nlp.thesaurus;

import com.google.gson.Gson;
import nlp.idioms.Idioms;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.io.*;
import java.util.*;

/**
 * Created by Mohammad-Ali on 12/05/2016.
 */
public class ThesaurusBuilder {

	static Thesaurus thesaurus;
	static Set<String> forget = new HashSet<>();

	public static void main(String[] args) throws IOException, InterruptedException {

		int type = 0;
		thesaurus = Thesaurus.load();
		switch (type) {
		case 0:
			wordThesaurusBuilder();
			break;
		case 1:
			phraseThesaurusBuilder();
			break;
		default:
			addTerm(new Term("approve", "approve", "VB", null, 1));
			addTerm(new Term("air conditioner", "air conditioner", "NN", null, 1));

		}
	}

	private static void saveAndSleep() throws InterruptedException {
		System.out.print("\rsize = " + thesaurus.thesaurus.size());
		Gson gson = new Gson();
		String s = gson.toJson(thesaurus);
		String path = "/home/may/Downloads/thesausus/theasurs-" + System.currentTimeMillis() / 60000 + ".txt";
		try {
			File file = new File(path);
			// System.out.println("Writer has started its job, path : " + path);
			file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(s);
			bw.flush();
			bw.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		// Thread.sleep(2000);
	}

	public static ThesaurusEntity thesaurusFinder(String phrase) throws IOException {

		String url = "http://www.freethesaurus.com/" + phrase.replace(" ", "+");
		// url = URLEncoder.encode(url, "utf-8");
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		//  request.setHeader("User-agent","Mediapartners-Google");
		HttpResponse response = client.execute(request);
// Get the response
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuilder result = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		Document document = Jsoup.parse(result.toString());

		return createThesaurusEntity(phrase, document);
	}

	private static ThesaurusEntity createThesaurusEntity(String phrase, Document document) {
		Elements sections = document.select("div#MainTxt section");
		ThesaurusEntity thesaurusEntity = new ThesaurusEntity(phrase);
		for (Element section : sections) {

			Elements elements = section.select(".TM");
			if (elements.size() == 0) {
				break;
			}
			Element element = elements.get(0);
			List<ThesaurusSense> thesaurusSenses = new ArrayList<>(element.children().size());
			for (Element child : element.children()) {
				Set<String> syns = new HashSet<>();
				child.select(".TSyn li").forEach(s -> syns.add(s.text()));

				Set<String> relatedWords = new HashSet<>();
				child.select(".TRel li").forEach(s -> relatedWords.add(s.text()));

				Set<String> antonyms = new HashSet<>();
				child.select(".TAnt li").forEach(s -> antonyms.add(s.text()));

				ThesaurusSense thesaurusSense = new ThesaurusSense(child.select("h3").text(), child.attr("data-part"), syns, relatedWords, antonyms);
				thesaurusSenses.add(thesaurusSense);
			}

			switch (section.attr("data-src")) {
			case "hc_thes":
				thesaurusEntity.addCollinsSense(thesaurusSenses);
				break;
			case "hm_thes":
				thesaurusEntity.addRogetsSense(thesaurusSenses);
				break;
			case "wn":
				thesaurusEntity.addWordNetSense(thesaurusSenses);
				break;
			default:
				System.out.println("not predicted source : " + section.attr("data-src"));
			}
		}


		return thesaurusEntity;
	}

	public static Thesaurus fromJson(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, Thesaurus.class);
	}

	public static void phraseThesaurusBuilder() {

		Idioms idioms = new Idioms();

		int counter = 0;
		for (String idiom : idioms.idioms) {
			idiom = idiom.toLowerCase();
			try {

				System.out.println("Progress = " + counter * 1.0 / idioms.idioms.size());
				if (!thesaurus.thesaurus.containsKey(idiom)) {
					boolean found = false;
					for (int i = 0; i < 5; i++) {
						ThesaurusEntity thesaurusEntity = thesaurusFinder(idiom);
						if (thesaurusEntity.getCollins().size() != 0 || thesaurusEntity.getWordNet().size() != 0 || thesaurusEntity.getRogets().size() != 0) {
							thesaurus.thesaurus.put(idiom, thesaurusEntity);
							saveAndSleep();
							found = true;
							break;
						}
					}
					if (!found)
						System.out.println("NOT FOUND : " + idiom);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void wordThesaurusBuilder() {
		String resourcePath = ThesaurusBuilder.class.getClassLoader().getResource("").getFile().replace("target/classes", "src/main/resources/dataset");
/*

		// 2012
		String[] dataset_test_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt",
				"surprise.OnWN.txt",
				"surprise.SMTnews.txt"
		};

		for (String dataset : dataset_test_2012) {
			String tempPath = "original/2012/test-gold/";
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);
		}

		String[] dataset_train_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt"
		};

		for (String dataset : dataset_train_2012) {
			String tempPath = "original/2012/train/";
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2013
		String[] dataset_2013 = new String[]{
				"FNWN.txt",
				"headlines.txt",
				"OnWN.txt"
		};

		for (String dataset : dataset_2013) {
			String tempPath = "original/2013/test-gs/";
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);

		}
*/

		// 2014
		String[] dataset_2014 = new String[]{
				"deft-forum.txt",
				"deft-news.txt",
				"headlines.txt",
				"images.txt",
				"OnWN.txt",
				"tweet-news.txt"
		};



		for (String dataset : dataset_2014) {
			String tempPath = "original/2014/test/";
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);

		}


		// 2015
		String[] dataset_2015 = new String[]{
				"answers-forums.txt",
				"answers-students.txt",
				"belief.txt",
				"headlines.txt",
				"images.txt"
		};

		for (String dataset : dataset_2015) {
			String tempPath = "original/2015/test/";
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);

		}
		// 2016
		String[] dataset_2016 = new String[]{
				"answer-answer.txt",
				"headlines.txt",
				"plagiarism.txt",
				"postediting.txt",
				"question-question.txt"
		};

		for (String dataset : dataset_2016) {
			String tempPath = "original/2016/test/";
			updateWordThesaurus(resourcePath + tempPath + "Database-" + dataset);

		}
	}

	private static void updateWordThesaurus(String path) {
		System.err.println(path);
		try {
			String inputLine;
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));

			while ((inputLine = inputReader.readLine()) != null) {
				SentencePair sentencePair = SentencePair.toSentencePair(inputLine);

				sentencePair.getSentence_1().getTerms().forEach(nlp.thesaurus.ThesaurusBuilder::addTerm);
				sentencePair.getSentence_2().getTerms().forEach(nlp.thesaurus.ThesaurusBuilder::addTerm);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void addTerm(Term term) {
		String lemma = term.getLemma().toLowerCase();
		String word = term.getWord().toLowerCase();
		try {
			ThesaurusEntity thesaurusEntity = null;
			if (!thesaurus.thesaurus.containsKey(lemma) && !forget.contains(lemma)) {
				thesaurusEntity = thesaurusFinder(lemma);
				if (thesaurusEntity.getCollins().size() != 0 || thesaurusEntity.getWordNet().size() != 0 || thesaurusEntity.getRogets().size() != 0) {
					thesaurus.thesaurus.put(lemma, thesaurusEntity);

				}
				else
				{
					forget.add(lemma);
					System.out.println("forget size = " + forget.size());
				}

			}

			if (!thesaurus.thesaurus.containsKey(word) && !forget.contains(word)) {
				thesaurusEntity = thesaurusFinder(word);
				if (thesaurusEntity.getCollins().size() != 0 || thesaurusEntity.getWordNet().size() != 0 || thesaurusEntity.getRogets().size() != 0) {
					thesaurus.thesaurus.put(word, thesaurusEntity);
				}
				else {
					forget.add(word);
					System.out.println("forget size = " + forget.size());
				}
			}

			saveAndSleep();
		}
		catch (Exception e) {
			{
				forget.add(word);
				forget.add(lemma);
				System.out.println("forget size = " + forget.size());
			}
			e.printStackTrace();
		}
	}
}