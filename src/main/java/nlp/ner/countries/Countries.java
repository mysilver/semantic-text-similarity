package nlp.ner.countries;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Countries {

	public Map<String,String> shortForm = new HashMap<>();
	public Map<String,String> countries = new HashMap<>();

	public Countries() {
		String[] locales = Locale.getISOCountries();

		for (String countryCode : locales) {

			Locale obj = new Locale("", countryCode);
			shortForm.put( obj.getCountry(), obj.getDisplayCountry().toLowerCase());
			countries.put(obj.getDisplayCountry().toLowerCase(), obj.getCountry());

		}
	}

	public boolean isCountry(String countryName) {
		countryName = countryName.toLowerCase();
		return shortForm.containsKey(countryName) || countries.containsKey(countryName);
	}

	public String toBriefName(String countryName) {
		return countries.get(countryName.toLowerCase());
	}

	public String toName(String briefName) {
		return countries.get(briefName.toLowerCase());
	}
}
