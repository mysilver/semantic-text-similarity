package nlp.stats;

import nlp.TextSplitter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Mohammad-Ali on 16/04/2016.
 */
public class DfReader {

    private static Pattern pattern = Pattern.compile("\t");
    private Long minDf = new Long(0);
    private long total = 0 ;
    private Map<String, Long> dfs = new HashMap<>();
    private Long max = new Long(0);

    public DfReader(String path) {
        init(path);
    }

    public DfReader(String path, Long minDf) {
        this.minDf = minDf;
        init(path);
    }

    private void init(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine())!=null) {
                String[] strings = pattern.split(line, 2);
                Long frequency = Long.valueOf(strings[1]);
                max = frequency > max ? frequency:max;
                total+= frequency;
                if (frequency < minDf)
                    break;

                dfs.put(strings[0], frequency);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.err.println("unigram-df list is ready");
    }

    public Long getDF(String word) {
        if (dfs.containsKey(word))
            return dfs.get(word);
        return minDf;
    }

    public double getIDF(String word) {
        Long df = dfs.get(word);

        if (df == null && word.indexOf(' ') == -1)
            df = minDf;

        if (word.indexOf(' ') != -1)
        {
            long sum = 0;
            List<String> tokens = TextSplitter.tokenize(word);
            for (String token : tokens) {
                Long aLong = dfs.get(token);
                if ( aLong == null)
                    aLong = minDf;
                sum += aLong;
            }
            df = sum / tokens.size() / tokens.size();
        }

        return Math.log((total - df + 0.5)/(df));
    }

    public double getNormalizedDF(String word) {
        Long df = dfs.get(word);
        if (df==null)
            df = minDf;

        return (float) df / max;
    }



}
