package nlp.stats;

import de.tudarmstadt.ukp.dkpro.core.api.resources.ResourceUtils;
import dkpro.similarity.algorithms.lexical.ngrams.CharacterNGramMeasure;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import semeval.tasks.sts.db.SentencePair;

import java.io.File;
import java.net.URL;
import java.util.*;


/**
 * Created by may on 4/4/16.
 */
public class CharNGramIdfGenerator {

	static final String LF = System.getProperty("line.separator");

	public static void main(String[] args) throws Exception {
		String idfGBaseDir = CharNGramIdfGenerator.class.getClassLoader().getResource("").getFile().replace("target/classes", "src/main/resources/charsNgrams");
		String database = CharNGramIdfGenerator.class.getClassLoader().getResource("").getFile() + "dataset/training/Database.txt";
		for (int i = 1; i < 10; i++) {
			computeIdfScores(database, idfGBaseDir + "idf_"+i+"ngrms", i);
		}
	}

	@SuppressWarnings("unchecked")
	public static void computeIdfScores(String database, String idfPath,int n)
			throws Exception
	{
		URL inputUrl = ResourceUtils.resolveLocation(database);
		List<String> lines = IOUtils.readLines(inputUrl.openStream(), "utf-8");

		System.out.println("Computing character " + n + "-grams");

		File outputFile = new File(idfPath);

		if (outputFile.exists())
		{
			System.out.println(" - skipping, already exists");
		}
		else
		{
			Map<String, Double> idfValues = new HashMap<String, Double>();

			CharacterNGramMeasure measure = new CharacterNGramMeasure(n, new HashMap<String, Double>());

			// Get n-gram representations of texts
			List<Set<String>> docs = new ArrayList<Set<String>>();

			for (String line : lines)
			{
				SentencePair sentencePair = SentencePair.toSentencePair(line);
				Set<String> ngrams = measure.getNGrams(sentencePair.getSentence_1().getText());
				docs.add(ngrams);

				ngrams = measure.getNGrams(sentencePair.getSentence_2().getText());
				docs.add(ngrams);

			}

			// Get all ngrams
			Set<String> allNGrams = new HashSet<String>();
			for (Set<String> doc : docs)
				allNGrams.addAll(doc);

			// Compute idf values
			for (String ngram : allNGrams)
			{
				double count = 0;
				for (Set<String> doc : docs)
				{
					if (doc.contains(ngram))
						count++;
				}
				idfValues.put(ngram, count);
			}

			// Compute the idf
			for (String lemma : idfValues.keySet())
			{
				double idf = Math.log10(lines.size() / idfValues.get(lemma));
				idfValues.put(lemma, idf);
			}

			// Store persistently
			StringBuilder sb = new StringBuilder();
			for (String key : idfValues.keySet())
			{
				sb.append(key + "\t" + idfValues.get(key) + LF);
			}
			FileUtils.writeStringToFile(outputFile, sb.toString());

			System.out.println(" - done");
		}
	}
}
