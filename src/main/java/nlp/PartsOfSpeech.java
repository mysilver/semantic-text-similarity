package nlp;

import de.tudarmstadt.ukp.dkpro.lexsemresource.Entity;
import edu.mit.jwi.item.POS;

/**
 * Created by Mohammad-Ali on 19/05/2016.
 */
public enum PartsOfSpeech {
    VERB,
    PHRASAL_VERB,
    NOUN,
    NOUN_PHRASE,
    ADJECTIVE,
    ADVERB,
    NONE;

    public static PartsOfSpeech corenlpToPartsOfSpeech(String standfordPOS) {

        if (standfordPOS == null)
            return NONE;

        if (standfordPOS.equals("VB-PH"))
            return PHRASAL_VERB;

        if (standfordPOS.equals("NN-PH"))
            return NOUN_PHRASE;

        if (standfordPOS.contains("JJ"))
            return ADJECTIVE;

        if (standfordPOS.contains("RB") || standfordPOS.contains("IN"))
            return ADVERB;

        if (standfordPOS.contains("VB") || standfordPOS.contains("MD"))
            return VERB;

        if (standfordPOS.contains("NN") || standfordPOS.contains("CD") || standfordPOS.contains("FW") || standfordPOS.contains("LS"))
            return NOUN;

        //if (pos.contains("DT") || pos.contains("IN")  || pos.contains("CC")  || pos.contains("CC"))
        return NONE;
        //FW LS  UH PRP PRP$ $ RP WP$
    }

    public static POS toWordNetPOS(String standfordPOS) {

        if (standfordPOS == null)
            return null;

        if (standfordPOS.contains("JJ"))
            return POS.ADJECTIVE;

        if (standfordPOS.contains("RB") || standfordPOS.contains("IN"))
            return POS.ADVERB;

        if (standfordPOS.contains("VB") || standfordPOS.contains("MD"))
            return POS.VERB;

        if (standfordPOS.contains("NN") || standfordPOS.contains("CD") || standfordPOS.contains("FW") || standfordPOS.contains("LS"))
            return POS.NOUN;

        //if (standfordPOS.contains("DT") || standfordPOS.contains("IN")  || standfordPOS.contains("CC")  || standfordPOS.contains("CC"))
        return null;
        //FW LS  UH PRP PRP$ $ RP WP$
    }

    public static Entity.PoS toDkproPos(String pos) {

        if (pos == null)
            return Entity.PoS.unk;

        if (pos.contains("JJ"))
            return Entity.PoS.adj;

        if (pos.contains("RB") || pos.contains("IN"))
            return Entity.PoS.adv;

        if (pos.contains("VB") || "MD".equals(pos))
            return Entity.PoS.v;

        if (pos.contains("NN") || pos.contains("CD") || pos.contains("FW") || pos.contains("LS"))
            return Entity.PoS.n;

        //if (pos.contains("DT") || pos.contains("IN"))
        return Entity.PoS.unk;
    }

    public static PartsOfSpeech thesaurusToPartsOfSpeech(String pos) {

        switch (pos) {
            case "noun":
                return NOUN;
            case "phrase":
                return NOUN_PHRASE;
            case "verb":
                return VERB;
            case "adj":
                return ADJECTIVE;
            case "adv":
                return ADVERB;
            case "conj":
            case "pronoun":
            case "determiner":
            case "prep":
            case "abbr":
            case "interj":
                return NONE;
            default:
                throw new UnsupportedOperationException(pos + " unfamiliar exception");
        }
    }
}



