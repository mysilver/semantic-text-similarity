package nlp.preprocessing;

import nlp.NLPTools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

/**
 * threadsafe
 */
public class StopWords {

	private final Set<String> stopwords =  new HashSet<>();

	public StopWords() {
		loadStopwords();
	}

	public boolean isStopWord(String word) {
		return word.startsWith("-") && word.endsWith("-") || stopwords.contains(word.toLowerCase());
	}

	static String stops =".(){}!?;'`:\",&/\\-_~''``*#><^ ...???!!!-----------";
	public boolean isSimpleStopWord(String lemma) {
		return lemma.startsWith("-") && lemma.endsWith("-") || stops.contains(lemma);
	}
	
	private void loadStopwords() {

		String file = null;
		try {
			file = stopWordPath();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine())!=null)
				stopwords.add(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println("stopword list is ready");
	}

	public static String stopWordPath() throws IOException {
		File f = new File("stopword.txt");
		if(f.exists() && !f.isDirectory()) {
			return f.getPath();
		}
		return NLPTools.class.getClassLoader().getResources("stopword.txt").nextElement().getFile();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String stopword : stopwords) {
			sb.append(stopword).append("\n");
		}
		return sb.toString();
	}

}
