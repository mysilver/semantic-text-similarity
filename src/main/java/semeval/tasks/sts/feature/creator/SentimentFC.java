package semeval.tasks.sts.feature.creator;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.PossibleValues;
import ml.dataset.feature.value.BooleanValue;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import ml.dataset.feature.value.ShortValue;
import semeval.tasks.sts.db.SentencePair;

import java.util.ArrayList;
import java.util.List;

public class SentimentFC implements FeatureCreator {

	@Override
	public List<FeatureValue> create(String line) {
		SentencePair sentencePair = SentencePair.toSentencePair(line);

		short sentiment_1 = sentencePair.getSentence_1().getSentiment();
		short sentiment_2 = sentencePair.getSentence_2().getSentiment();
		List<FeatureValue> features = new ArrayList<>(7);
		/*features.add(new ShortValue(sentiment_1));
		features.add(new ShortValue(sentiment_2));*/
		features.add(new FloatValue((5 - (Math.abs(sentiment_1 - sentiment_2)-1))/5.0));
		// features.add(new BooleanValue(sentiment_1 == sentiment_2));
		/*features.add(new BooleanValue(sentiment_1 < 2 && sentiment_2 <2));
		features.add(new BooleanValue(sentiment_1 > 2 && sentiment_2 >2));
		features.add(new BooleanValue(sentiment_1 == 2 && sentiment_2 ==2));*/
		return features;
	}

	@Override
	public List<FeatureAttribute> attributes() {
		List<FeatureAttribute> ret = new ArrayList<>();
		//List<String> possibleValues = PossibleValues.intValues(0,4);
		/*ret.add(new FeatureAttribute("sentiment-1", possibleValues));
		ret.add(new FeatureAttribute("sentiment-2", possibleValues));*/
		ret.add(new FeatureAttribute("SentimentDiff", FeatureAttribute.AttributeType.NUMERIC));
		// ret.add(new FeatureAttribute("sentimentEquality", PossibleValues.booleanValues()));
		/*ret.add(new FeatureAttribute("sentiment-negative", PossibleValues.booleanValues()));
		ret.add(new FeatureAttribute("sentiment-positive", PossibleValues.booleanValues()));
		ret.add(new FeatureAttribute("sentiment-Neutral", PossibleValues.booleanValues()));*/
		return ret;
	}
}
