package semeval.tasks.sts.feature.creator;

import edu.stanford.nlp.stats.ClassicCounter;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.stats.Counters;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.ShortValue;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SynVectorFC implements FeatureCreator {

    @Override
    public List<FeatureValue> create(String line) {

        SentencePair sentencePair = SentencePair.toSentencePair(line);

        Set<String> sent1 = sentencePair.getSentence_1().synonymSet();
        Set<String> sent2 = sentencePair.getSentence_2().synonymSet();

        int i = 0;
        for (String w : sent2) {
            if (sent2.contains(w))
                i++;
        }

        sent1.addAll(sent2);
        List<FeatureValue> ret = new ArrayList<>(6);
        try {
            Counter<String> sen1Vector = toVector(sentencePair.getSentence_1());
            Counter<String> sen2Vector = toVector(sentencePair.getSentence_2());
            // double cosine = Counters.cosine(sen1Vector, sen2Vector);
            ////double klDivergence = Counters.klDivergence(sen1Vector, sen2Vector);
            //// double pearsonsCorrelationCoefficient = Counters.pearsonsCorrelationCoefficient(sen1Vector, sen2Vector);
            //// double spearmanRankCorrelation = Counters.spearmanRankCorrelation(sen1Vector, sen2Vector);
            //// double crossEntropy = Counters.crossEntropy(sen1Vector, sen2Vector);
            double docProduct = Counters.dotProduct(sen1Vector, sen2Vector);
            double jaccardCoefficient = Counters.jaccardCoefficient(sen1Vector, sen2Vector);
            double jensenShannonDivergence = Counters.jensenShannonDivergence(sen1Vector, sen2Vector);
            double entropyDiff = Math.abs(Counters.entropy(sen1Vector) - Counters.entropy(sen2Vector));
            ret.add(new ShortValue((short) (i*100/(sent1.size()))));
            //ret.add(new ShortValue((short) (100 * cosine)));
           //// ret.add(String.valueOf((int)(100 * klDivergence)));
           //// ret.add(String.valueOf(pearsonsCorrelationCoefficient));
           //// ret.add(String.valueOf(spearmanRankCorrelation));
           //// ret.add(String.valueOf((int)(100 * crossEntropy)));
            ret.add(new ShortValue((short)(100 * docProduct)));
            ret.add(new ShortValue((short)(100 * jaccardCoefficient)));
            ret.add(new ShortValue((short)(100 * jensenShannonDivergence)));
            ret.add(new ShortValue((short)(100 * entropyDiff)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return ret;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List<FeatureAttribute> ret = new ArrayList<>();
                
        ret.add(new FeatureAttribute("synSet", FeatureAttribute.AttributeType.NUMERIC));
        // ret.add(new FeatureAttribute("cosine", FeatureAttribute.AttributeType.NUMERIC));
       // ret.add(new FeatureAttribute("klDivergence-"+ignoreStopwords, FeatureAttribute.AttributeType.NUMERIC));
      //  ret.add(new FeatureAttribute("pearsonsCorrelationCoefficient-"+ignoreStopwords, FeatureAttribute.AttributeType.NUMERIC));
      //  ret.add(new FeatureAttribute("spearmanRankCorrelation-"+ignoreStopwords, FeatureAttribute.AttributeType.NUMERIC));
       // ret.add(new FeatureAttribute("crossEntropy-"+ignoreStopwords, FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("docProduct", FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("jaccardCoefficient", FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("jensenShannonDivergence", FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("entropyDiff", FeatureAttribute.AttributeType.NUMERIC));
        return ret;
    }

    public Counter<String> toVector(Sentence sentence) throws IOException {

        Counter<String> ret = new ClassicCounter<>();
        for (Term term : sentence.getTerms()) {
            ret.incrementCount(term.getLemma(), 1);
        }
        return  ret;
    }

}
