package semeval.tasks.sts.db;

import com.google.gson.Gson;

public class SentencePair {

	private final Sentence Sentence_1;
	private final Sentence Sentence_2;
	private final float className;

	public SentencePair(Sentence sentence_1, Sentence sentence_2, float className) {
		Sentence_1 = sentence_1;
		Sentence_2 = sentence_2;
		this.className = className;
	}

	public Sentence getSentence_1() {
		return Sentence_1;
	}

	public Sentence getSentence_2() {
		return Sentence_2;
	}

	public static SentencePair toSentencePair(String line) {
		Gson gson = new Gson();
		SentencePair sentencePair = gson.fromJson(line, SentencePair.class);
		if (sentencePair.getSentence_1().getText().length() >= sentencePair.getSentence_1().getText().length())
			return sentencePair;
		return new SentencePair(sentencePair.getSentence_2(), sentencePair.getSentence_1(), sentencePair.getClassName());
	}

	public float getClassName() {
		return className;
	}

	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	@Override
	public String toString() {
		return
				"ClassName:\t" + className +
				"\tSentiments:\t(" + Sentence_1.getSentiment() + "," + Sentence_2.getSentiment() + ")" +
				"\nSentence #1:\t" + Sentence_1.getWords() + "\tLemma:\t" + Sentence_1.getLemmas() +
				"\nSentence #2:\t" + Sentence_2.getWords() + "\tLemma:\t" + Sentence_2.getLemmas();/*
				"\nLemma_1:\t" + Sentence_1.getLemma() +
				"\nLemma_2:\t" + Sentence_2.getLemma() +*/
				/*"\nNameEntity_1:\t" + Sentence_1.getNameEntities() +
				"\nNameEntity_2:\t" + Sentence_2.getNameEntities() +*/

	}
}
