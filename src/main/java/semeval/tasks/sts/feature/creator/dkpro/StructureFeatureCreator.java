package semeval.tasks.sts.feature.creator.dkpro;

import dkpro.similarity.algorithms.api.SimilarityException;
import dkpro.similarity.algorithms.api.TextSimilarityMeasure;
import dkpro.similarity.algorithms.structure.*;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StructureFeatureCreator implements FeatureCreator {

    List<TextSimilarityMeasure> measures = new ArrayList<>();

    public StructureFeatureCreator() throws SimilarityException, IOException {

        // measures.add(new PosNGramContainmentMeasure(i)); SIMULATED
        // measures.add(new PosNGramJaccardMeasure(i)); SIMULATED
        // measures.add(new StopwordNGramContainmentMeasure(1, stopWords)); NOT USEFUL
        measures.add(new TokenPairDistanceMeasure());
        //measures.add(new TokenPairOrderingMeasure());
    }

    @Override
    public List<FeatureValue> create(String line) {
        List<FeatureValue> ret = new ArrayList<>();

        SentencePair sentencePair = SentencePair.toSentencePair(line);

        for (TextSimilarityMeasure measure : measures) {
            try {
                double score = measure.getSimilarity(sentencePair.getSentence_1().getLemmas(), sentencePair.getSentence_2().getLemmas());
                ret.add(new FloatValue((float) score));
            } catch (Exception e) {
                System.out.println(measure);
                e.printStackTrace();
            }
        }
        return ret;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List<FeatureAttribute> ret = new ArrayList<>();
        for (TextSimilarityMeasure measure : measures) {
            ret.add(new FeatureAttribute(measure.getName(), FeatureAttribute.AttributeType.NUMERIC));
        }

        return ret;
    }
}
