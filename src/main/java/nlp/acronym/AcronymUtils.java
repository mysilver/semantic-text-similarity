package nlp.acronym;

import edu.mit.jwi.item.POS;
import nlp.PartsOfSpeech;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AcronymUtils {

    public static Pattern patternLowerCase = Pattern.compile("\\p{javaLowerCase}+");
    public static Pattern patternUpperCase = Pattern.compile("\\p{javaUpperCase}+");
    static int maxLength = 5;

    public static boolean isAcronym(String word, POS pos) {

        if (pos != POS.NOUN )
            return false;

        if (word.length() > maxLength)
            return false;

        if (word.startsWith("-") && word.endsWith("-"))
            return false;

        if (word.replace("/", "").replace(".", "").length() < 2)
            return false;

        Matcher matcherLowerCase = patternLowerCase.matcher(word);
        Matcher matcherUpperCase = patternUpperCase.matcher(word);
        return !matcherLowerCase.find() && matcherUpperCase.find();
    }

    public static boolean appearsInSentence(String acronym, Sentence sentence) {

        acronym = acronym.replace("/", "").replace(".", "");

        StringBuilder initials = new StringBuilder();
        for (Term term : sentence.getTerms()) {
            initials.append(term.getWord().charAt(0));
        }

        String initialsUperCase = initials.toString().toUpperCase();
        if( initialsUperCase.contains(acronym))
            return true;

        return false;
    }

    public static boolean appearsInAcronyms(Term include, SentencePair sentencePair) {

        List<List<Term>> acronyms = acronyms(sentencePair);

        for (List<Term> terms : acronyms) {
            if (terms.contains(include))
                return true;
        }

        return false;
    }

    public static List<List<Term>> acronyms(SentencePair sentencePair) {
        List<List<Term>> ret = new ArrayList<>();
        for (Term term : sentencePair.getSentence_1().getTerms())
            if (isAcronym(term.getWord(), PartsOfSpeech.toWordNetPOS(term.getPos()))) {
                String acronym = term.getWord().replace("/", "").replace(".", "");
                for (int i = 0; i < sentencePair.getSentence_2().getTerms().size() - acronym.length() + 1; i++) {
                    boolean found = false;
                    for (int j = 0; j < acronym.length(); j++) {
                        if (acronym.charAt(j) != sentencePair.getSentence_2().getTerms().get(i+j).getWord().toUpperCase().charAt(0))
                            found = true;
                    }
                    if (!found) {
                        List<Term> terms = new ArrayList<>();
                        for (int j = 0; j < acronym.length(); j++) {
                            terms.add(sentencePair.getSentence_2().getTerms().get(i+j));
                        }
                        ret.add(terms);
                    }
                }
            }

        for (Term term : sentencePair.getSentence_2().getTerms())
            if (isAcronym(term.getWord(), PartsOfSpeech.toWordNetPOS(term.getPos()))) {
                String acronym = term.getWord().replace("/", "").replace(".", "");
                for (int i = 0; i < sentencePair.getSentence_1().getTerms().size() - acronym.length() + 1; i++) {
                    boolean found = false;
                    for (int j = 0; j < acronym.length(); j++) {
                        if (acronym.charAt(j) != sentencePair.getSentence_1().getTerms().get(i+j).getWord().toUpperCase().charAt(0))
                            found = true;
                    }
                    if (!found) {
                        List<Term> terms = new ArrayList<>();
                        for (int j = 0; j < acronym.length(); j++) {
                            terms.add(sentencePair.getSentence_1().getTerms().get(i+j));
                        }
                        ret.add(terms);
                    }
                }
            }

        return ret;
    }
}
