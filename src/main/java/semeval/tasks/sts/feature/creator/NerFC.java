package semeval.tasks.sts.feature.creator;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import nlp.ner.NERTool;
import semeval.tasks.sts.db.SentencePair;

import java.util.ArrayList;
import java.util.List;

public class NerFC implements FeatureCreator {

    @Override
    public List<FeatureValue> create(String line) {
        List<FeatureValue> ret = new ArrayList<>();
        SentencePair sentencePair = SentencePair.toSentencePair(line);

        float match = 0;
        for (NERTool.NameEntity nameEntity : sentencePair.getSentence_1().getNameEntities()) {
            for (NERTool.NameEntity entity : sentencePair.getSentence_2().getNameEntities()) {
                if (entity.equals(nameEntity)) {
                    match++;
                    break;
                }
            }
        }

        float match2 = 0;
        for (NERTool.NameEntity nameEntity : sentencePair.getSentence_1().getNameEntities()) {
            for (NERTool.NameEntity entity : sentencePair.getSentence_2().getNameEntities()) {
                if (entity.getName().equals(nameEntity.getName())) {
                    match2++;
                    break;
                }
            }
        }

        int size_1 = sentencePair.getSentence_1().getNameEntities().size();
        int size_2 = sentencePair.getSentence_2().getNameEntities().size();
        // ret.add(new ShortValue(size_1 - size_2));
        // ret.add(new ShortValue(size_2));
//        double lengthNorm = Math.log((sentencePair.getSentence_1().getTerms().size() + sentencePair.getSentence_1().getTerms().size()) / 2);
         double lengthNorm = 1;

        if (size_1 == 0 && size_2 == 0) {
            ret.add(new FloatValue(lengthNorm));
        }
        else {
           // ret.add(new ShortValue(Math.min(match * 100 / (size_1 + size_2 - match), 100)));
                //ret.add(new FloatValue(match / (sentencePair.getSentence_1().getTerms().size() + sentencePair.getSentence_2().getTerms().size() - match)));
                //ret.add(new FloatValue(lengthNorm * match / (size_1 + size_2 - match)));
            float v = match / (size_1 + size_2 - match);
            ret.add(new FloatValue(lengthNorm * Math.log(v+0.001)));

            /*float normalized = size_1 / (float) sentencePair.getSentence_1().getTerms().size();
            normalized *= size_2 / (float) sentencePair.getSentence_2().getTerms().size();
            normalized *=matchness;
            ret.add(new FloatValue(normalized));*/
        }
/*

        Map<NERTool.NameEntity.Type, List<String>> nameEntitiesByType_Snt1 = new HashMap<>();
        for (NERTool.NameEntity.Type type : NERTool.NameEntity.Type.values()) {
            nameEntitiesByType_Snt1.put(type, new ArrayList<>());
        }

        for (NERTool.NameEntity nameEntity : sentencePair.getSentence_1().getNameEntities()) {
            nameEntitiesByType_Snt1.get(nameEntity.getName()).add(nameEntity.getValue());
        }

        Map<NERTool.NameEntity.Type, List<String>> nameEntitiesByType_Snt2 = new HashMap<>();
        for (NERTool.NameEntity.Type type : NERTool.NameEntity.Type.values()) {
            nameEntitiesByType_Snt2.put(type, new ArrayList<>());
        }

        for (NERTool.NameEntity nameEntity : sentencePair.getSentence_2().getNameEntities()) {
            nameEntitiesByType_Snt2.get(nameEntity.getName()).add(nameEntity.getValue());
        }

        for (NERTool.NameEntity.Type type : NERTool.NameEntity.Type.values()) {
            List<String> entities_1 = nameEntitiesByType_Snt1.get(type);
            List<String> entities_2 = nameEntitiesByType_Snt2.get(type);
            ret.add(new ShortValue(entities_1.size()));
            ret.add(new ShortValue(entities_2.size()));
            int matchCount = 0;

            for (String term : entities_1) {
                if (entities_2.contains(term))
                    matchCount++;
            }

            if (entities_2.size() == 0 && entities_1.size() == 0)
                ret.add(new ShortValue(-1));
            else
                ret.add(new ShortValue(Math.min(matchCount*100 / (entities_1.size() + entities_2.size() - matchCount), 100)));
        }
*/

        return ret;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List<FeatureAttribute> ret = new ArrayList<>();

        //ret.add(new FeatureAttribute("Num-Of_NE_1", FeatureAttribute.AttributeType.NUMERIC));
        //ret.add(new FeatureAttribute("Num-Of-NE_2", FeatureAttribute.AttributeType.NUMERIC));
        //ret.add(new FeatureAttribute("NE_Equality", PossibleValues.intValues(-1,100)));
        //ret.add(new FeatureAttribute("NE_Size", FeatureAttribute.AttributeType.NUMERIC));
        //ret.add(new FeatureAttribute("NameEntityKey", FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("NameEntityFull", FeatureAttribute.AttributeType.NUMERIC));
       // ret.add(new FeatureAttribute("NE_Normalized", FeatureAttribute.AttributeType.NUMERIC));
/*
        for (NERTool.NameEntity.Type type : NERTool.NameEntity.Type.values()) {
            ret.add(new FeatureAttribute("Num-Of-"+type+"_1", FeatureAttribute.AttributeType.NUMERIC));
            ret.add(new FeatureAttribute("Num-Of-"+type+"_2", FeatureAttribute.AttributeType.NUMERIC));
            ret.add(new FeatureAttribute(type+"-Equality", PossibleValues.intValues(-1,100)));
        }*/

        return ret;
    }
}
