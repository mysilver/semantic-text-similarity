package ml.dataset.feature.value;

public class ShortValue implements FeatureValue {

    private final short value;

    public ShortValue(short value) {
        this.value = value;
    }

    public ShortValue(int value) {
        this.value = (short) value;
    }

    @Override
    public String value() {
        return String.valueOf(value);
    }
}
