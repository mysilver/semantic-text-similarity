package nlp;

/**
 * Created by may on 6/5/16.
 */
public class StringBuilderUtil {

    /**
     * Trims a {@link StringBuilder} object and returns a {@link String}
     */
    public static String trim(StringBuilder sb) {
        int first, last;

        for (first = 0; first < sb.length(); first++)
            if (!Character.isWhitespace(sb.charAt(first)))
                break;

        for (last = sb.length(); last > first; last--)
            if (!Character.isWhitespace(sb.charAt(last - 1)))
                break;

        return sb.substring(first, last);
    }
}
