package nlp.idioms;

import edu.stanford.nlp.util.Pair;
import nlp.thesaurus.Thesaurus;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.Term;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class Idioms {

    public Set<String> idioms = new HashSet<>(10000);
   // private NLPTools nlpTools;

    public Idioms() {
        File f = new File("idioms.txt");
        if(!f.exists() || f.isDirectory())
            f = new File(Idioms.class.getResource("/idioms.txt").getPath());

        try (BufferedReader br = new BufferedReader(new FileReader(f.getPath()))) {
            String line;
            while ((line = br.readLine())!=null)
                idioms.add(preprocess(line));
            //nlpTools = new NLPTools();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.err.println("Idiom list is ready");

    }

    private static String preprocess(String text){
        return text.toLowerCase().replace('-',' ').replace(',',' ').trim();
    }

    Set<String> availblePhrases = Thesaurus.load().getPhrases().keySet();
    public Sentence replacePhrases(Sentence sentence) {
        // VerbIndex, <Verb, RestIndex>
        Map<Integer, Pair<List<String>, Integer>> candidates = new HashMap<>();

        for (String phrase : idioms) {

            if (!availblePhrases.contains(phrase))
                continue;
            //List<String> terms = nlpTools.tokenize(phrase);
            List<String> terms = Arrays.asList(phrase.split(" "));

            if (terms.size() < 2)
                continue;

            String phraseLemma = String.join(" ", terms).toLowerCase();
            if (!sentence.getLemma().toLowerCase().contains(phraseLemma) && !sentence.getText().toLowerCase().contains(phraseLemma))
                continue;

            for (int i = 0; i < sentence.getTerms().size(); i++) {

                int start = -1;

                for (int j = 0; j < terms.size() && (j + i) < sentence.getTerms().size(); j++) {
                    if (!sentence.getTerms().get(i+j).getLemma().equalsIgnoreCase(terms.get(j))
                            && !sentence.getTerms().get(i+j).getWord().equalsIgnoreCase(terms.get(j))) {
                        start = -1;
                        break;
                    }
                    start = i;
                }
                if (start != -1) {
                    Pair<List<String>, Integer> listIntegerPair = candidates.get(start);
                    if (listIntegerPair == null || listIntegerPair.first().size() < terms.size())
                        candidates.put(start, new Pair<>(terms, start));
                    //System.out.println(start + ", verb : " + phrase.getParts() + ", sentence : " + sentence.getLemma());
                }

            }

        }

        List<Term> newTerms = sentence.getTerms();
        for (Map.Entry<Integer, Pair<List<String>, Integer>> listPairEntry : candidates.entrySet()) {
            newTerms.get(listPairEntry.getKey()).setWord(String.join(" ", listPairEntry.getValue().first()));
            newTerms.get(listPairEntry.getKey()).setLemma(newTerms.get(listPairEntry.getKey()).getWord());
            newTerms.get(listPairEntry.getKey()).setPos("NN-PH");
            //newTerms.get(listPairEntry.getKey()).setSynonyms(nlpTools.getWordNet().getSynset(newTerms.get(listPairEntry.getKey()).getWord(), POS.NOUN));
        }

        for (Pair<List<String>, Integer> listPairEntry : candidates.values()) {
            for (int i = listPairEntry.second()+1; i < listPairEntry.first().size() + listPairEntry.second(); i++) {
                newTerms.get(i).setWord(null);
            }
        }

        List<Term> finalTerms = new ArrayList<>();
        for (Term newTerm : newTerms) {
            if (newTerm.getWord() != null)
                finalTerms.add(newTerm);
        }

        return new Sentence(sentence.getText(),finalTerms, sentence.getSentiment(),sentence.getNameEntities());
    }

    public static void main(String[] args) {

        List<Term> terms = new ArrayList<>();
        terms.add(new Term("Microwave", "Microwave", "", null, 0));
        terms.add(new Term("would", "would", "", null, 0));
        terms.add(new Term("be", "be", "", null, 0));
        terms.add(new Term("your", "you", "", null, 0));
        terms.add(new Term("best", "best", "", null, 0));
        terms.add(new Term("bet", "bet", "", null, 0));
        Sentence sentence  = new Sentence("Microwave would be your best bet.", terms, (short) 1, null);
        System.out.println(new Idioms().replacePhrases(sentence).getWords());

    }
}
