package ml.dataset.print.formatter;

import ml.dataset.DataSet;
public interface DataSetFormatter {

    String format(DataSet dataSet);
}
