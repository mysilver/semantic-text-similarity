package nlp;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import edu.mit.jwi.item.POS;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.stats.ClassicCounter;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import nlp.ner.NERTool;
import nlp.preprocessing.StopWords;
import nlp.stats.DfReader;
import nlp.synonyms.SynsetBuilder;
import nlp.thesaurus.Thesaurus;
import nlp.wordnet.WordNet;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class NLPTools {

    private Map<String, Set<String>> SYNSET;
    private static Thesaurus thesaurus = Thesaurus.load();
    private StopWords stopWords;
    private StanfordCoreNLP nlp;
    private NERTool nerTool;
    private WordNet wordNet;
    private DfReader dfReader;
    private static char tab = '\t';
    private final Pattern patternSpace = Pattern.compile(" ");
    private final WordSimilarity wordSimilarity;

    LoadingCache<String, Annotation> annotationCache = CacheBuilder.newBuilder()
            .maximumSize(20)
            .expireAfterWrite(10, TimeUnit.HOURS)
            .build(new CacheLoader<String, Annotation>() {
                       @Override
                       public Annotation load(String sentence) throws Exception {
                           return nlp.process(sentence);
                       }
                   }
            );

    public NLPTools() throws IOException {
        Properties properties = new Properties();
        properties.put("annotators", "tokenize,ssplit,pos,lemma,parse,sentiment,ner,regexner");
        this.nlp = new StanfordCoreNLP(properties);
        wordNet = new WordNet();
        stopWords = new StopWords();
        nerTool = new NERTool(this.nlp);
        wordSimilarity = new WordSimilarity(this);
    }

    public NLPTools(String dfFile, String synsetPath, boolean all) throws IOException {
        this();
        if (all) {
            dfReader = new DfReader(dfFile, 200l);
            stopWords = new StopWords();
            SYNSET = SynsetBuilder.toSynset(synsetPath);
        }
    }

    public NLPTools(String dfFile, String synsetPath) throws IOException {
        dfReader = new DfReader(dfFile, 200l);
        stopWords = new StopWords();
        SYNSET = SynsetBuilder.toSynset(synsetPath);
        wordSimilarity = new WordSimilarity(this);
    }

    public String[] toSentences(String paragraph) throws ExecutionException {
        try {
            String[] ret = new String[2];
            int beginingOf2thSentence = paragraph.indexOf(tab);
            int beginingOf3thSentence = paragraph.length();
            ret[0] = process(paragraph.substring(0, beginingOf2thSentence));
            for (int i = beginingOf2thSentence + 1; i < paragraph.length(); i++) {
                if (paragraph.charAt(i) == tab) {
                    beginingOf3thSentence = i;
                    break;
                }
            }
            ret[1] = process(paragraph.substring(beginingOf2thSentence + 1, beginingOf3thSentence));
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String process(String text) {

        if (isNewsTitle(text))
            return Character.toUpperCase(text.charAt(0)) + text.substring(1, text.length()).toLowerCase();

        return text;
    }

    private boolean isNewsTitle(String text) {
        int counter = 0;

        List<String> tokenize = TextSplitter.tokenize(text);

        for (String s : tokenize) {
            if (Character.isUpperCase(s.charAt(0)))
                counter++;
        }

        return counter > tokenize.size() / 2.0;
    }

    public List<CoreLabel> toCoreLabels(String sentence) {
        Annotation annotation = null;
        try {
            annotation = annotationCache.get(sentence);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return annotation != null ? annotation.get(CoreAnnotations.TokensAnnotation.class) : null;
    }

    public List<String> tokenize(String sentence) {
        Annotation annotation = null;
        try {
            annotation = annotationCache.get(sentence);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (annotation == null)
            return null;
        List<CoreLabel> coreLabels = annotation.get(CoreAnnotations.TokensAnnotation.class);

        List<String> ret = new ArrayList<>();
        if (coreLabels != null) {
            for (CoreLabel coreLabel : coreLabels) {
                String lemma = coreLabel.get(CoreAnnotations.LemmaAnnotation.class);
                ret.add(lemma);
            }
        }
        return ret;
    }

    public Set<String> toWordSet(String sentence, boolean expandToSynonyms) {
        Set<String> ret = new HashSet<>(sentence.length());
        Annotation annotation = null;
        try {
            annotation = annotationCache.get(sentence);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        List<CoreLabel> coreLabels = annotation != null ? annotation.get(CoreAnnotations.TokensAnnotation.class) : null;

        if (coreLabels != null) {
            for (CoreLabel coreLabel : coreLabels) {
                String lemma = coreLabel.get(CoreAnnotations.LemmaAnnotation.class);
                String pos = coreLabel.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                if (expandToSynonyms)
                    ret.addAll(getSynset(lemma, pos));
                else
                    ret.add(lemma);
            }
        }
        return ret;
    }

    public boolean isStopWord(String word) {
        return stopWords.isStopWord(word);
    }

    public boolean isSimpleStopWord(String lemma) {
        return stopWords.isSimpleStopWord(lemma);
    }

    public Set<String> getSynset(String word, String pos) {
        POS pos1 = PartsOfSpeech.toWordNetPOS(pos);
        if (pos1 != null)
            return wordNet.getSynset(word, pos1);
        return new HashSet<>(0);
    }

    public Set<String> getSynset(String lemma) {
        Set<String> strings = SYNSET.get(lemma);
        return strings == null ? new HashSet<>(0) : strings;
    }

    public int findSentiment(String text) {

        int mainSentiment = 0;
        if (text != null && text.length() > 0) {
            int longest = 0;
            Annotation annotation = null;
            try {
                annotation = annotationCache.get(text);
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            assert annotation != null;
            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
                int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
                String partText = sentence.toString();
                if (partText.length() > longest) {
                    mainSentiment = sentiment;
                    longest = partText.length();
                }
            }
        }

        return mainSentiment;
    }

    public Counter<String> toVector(String sentence, boolean removeStopwords) throws IOException {
        String[] words = patternSpace.split(sentence);
        Counter<String> ret = new ClassicCounter<>();
        for (String word : words) {
            if (removeStopwords && isStopWord(word))
                continue;
            ret.incrementCount(word, 1);
        }
        return ret;
    }

    public List<NERTool.NameEntity> nerTokens(String sentence) {
        return this.nerTool.nerTokenize(sentence);
    }

    public DfReader getDfReader() {
        return dfReader;
    }

    public WordNet getWordNet() {
        return wordNet;
    }

    public Thesaurus getThesaurus() {
        return thesaurus;
    }

    public List<Term> removeStopWords(List<Term> terms, boolean simpleMode) {
        List<Term> words1WithoutSw = new ArrayList<>();
        for (Term s : terms) {
            if (simpleMode) {
                if (!isSimpleStopWord(s.getLemma()))
                    words1WithoutSw.add(s);
            } else {
                if (!isStopWord(s.getLemma()))
                    words1WithoutSw.add(s);
            }
        }
        return words1WithoutSw;
    }

    public int distance(List<Term> terms_1, List<Term> terms_2) {

        /*terms_1 = removeStopWords(terms_1, true);
        terms_2 = removeStopWords(terms_2, true);*/

        /*List<Term> terms_1 = sentence1.getTerms();
        List<Term> terms_2 = sentence2.getTerms();*/

        int len1 = terms_1.size();
        int len2 = terms_2.size();

        // len1+1, len2+1, because finally return dp[len1][len2]
        int[][] dp = new int[len1 + 1][len2 + 1];

        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }

        for (int j = 0; j <= len2; j++) {
            dp[0][j] = j;
        }

        //iterate though, and check last char
        for (int i = 0; i < len1; i++) {
            Term c1 = terms_1.get(i);
            for (int j = 0; j < len2; j++) {
                Term c2 = terms_2.get(j);

                //if last two chars equal
                WordSimilarity.Type similarity = wordSimilarity.similarity(c1, c2);
                if (similarity == WordSimilarity.Type.EXACT_MATCH || similarity == WordSimilarity.Type.MAJOR_MATCH /*|| similarity == WordSimilarity.Type.IN_SYNONYM_SET_L1*/) {
                    //update dp value for +1 length
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    int replace = dp[i][j] + 1;
                    int insert = dp[i][j + 1] + 1;
                    int delete = dp[i + 1][j] + 1;

                    int min = replace > insert ? insert : replace;
                    min = delete > min ? min : delete;
                    dp[i + 1][j + 1] = min;
                }
            }
        }

        return dp[len1][len2];
    }

    public List<Term> removeTheSameWords(List<Term> terms_1, List<Term> terms_2) {
        List<Term> ret = new ArrayList<>(terms_1.size());

        for (Term term1 : terms_1) {
            boolean found = false;
            for (Term term2 : terms_2) {
                WordSimilarity.Type similarity = wordSimilarity.similarity(term1, term2);
                if (similarity == WordSimilarity.Type.EXACT_MATCH || similarity == WordSimilarity.Type.MAJOR_MATCH /*|| similarity == WordSimilarity.Type.IN_SYNONYM_SET_L1*/) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                ret.add(term1);
            }
        }
        return ret;
    }

    public NERTool.NameEntity getNameEntity(Term term1, Sentence sentence) {
        for (NERTool.NameEntity nameEntity : sentence.getNameEntities()) {
            if (nameEntity.getValue().equalsIgnoreCase(term1.getWord()))
                return nameEntity;
        }
        return null;
    }

    public WordSimilarity.Type similarity(Term term1, Term term2) {
        return wordSimilarity.similarity(term1, term2);
    }

    public boolean phrasalSynonym(Term term, SentencePair sentencePair) {
        try {
            return wordSimilarity.phrasalSynonym(term, sentencePair);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return false;
        }
    }

    String[] negativityWords = new String[]{"not", "never", "no", "neither", "none"};

    public boolean blindSentiment(SentencePair sentencePair) {

        boolean sen1Neg = false;
        boolean sen2Neg = false;
        for (Term term1 : sentencePair.getSentence_1().getTerms()) {
            for (String negativeWord : negativityWords)
                if (term1.getLemma().equals(negativeWord)) {
                    sen1Neg = !sen1Neg;
                    break;
                }
            for (Term term2 : sentencePair.getSentence_2().getTerms()) {
                if (similarity(term1, term2) == WordSimilarity.Type.ANTONYMS) {
                    sen1Neg = !sen1Neg;
                    break;
                }
            }
        }

        for (Term term2 : sentencePair.getSentence_2().getTerms()) {
            for (String negativeWord : negativityWords)
                if (term2.getLemma().equals(negativeWord)) {
                    sen2Neg = !sen2Neg;
                    break;
                }
            for (Term term1 : sentencePair.getSentence_1().getTerms()) {
                if (similarity(term1, term2) == WordSimilarity.Type.ANTONYMS) {
                    sen2Neg = !sen2Neg;
                    break;
                }
            }
        }

        return sen1Neg == sen2Neg;
    }

}
