package semeval.tasks.sts.db;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import nlp.NLPTools;
import misc.StringPair;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class SentencePairWriter {

	public static Pattern pattern = Pattern.compile("\t");
	static int NUM_OF_THREADS = 2;
	private static NLPTools nlpTools;

	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
		original();
	}

	private static void original() throws InterruptedException, ExecutionException, IOException {

		String path = SentencePairWriter.class.getClassLoader().getResource("").getFile().replace("target/classes", "files");
		String resourcePath = SentencePairWriter.class.getClassLoader().getResource("").getFile().replace("target/classes", "src/main/resources/dataset");
		String stsInput = "STS.input.";
		String stsGs = "STS.gs.";

/*
		// 2012
		String[] dataset_test_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt",
				"surprise.OnWN.txt",
				"surprise.SMTnews.txt"
		};

		for (String dataset : dataset_test_2012) {
			String tempPath = "original/2012/test-gold/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-"+dataset);
		}

		String[] dataset_train_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt"
		};

		for (String dataset : dataset_train_2012) {
			String tempPath = "original/2012/train/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-"+dataset);

		}

		// 2013
		String[] dataset_2013 = new String[]{
				"FNWN.txt",
				"headlines.txt",
				"OnWN.txt"
		};

		for (String dataset : dataset_2013) {
			String tempPath = "original/2013/test-gs/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-"+dataset);

		}

		// 2014
		String[] dataset_2014 = new String[]{
				"deft-forum.txt",
				"deft-news.txt",
				"headlines.txt",
				"images.txt",
				"OnWN.txt",
				"tweet-news.txt"
		};

		for (String dataset : dataset_2014) {
			String tempPath = "original/2014/test/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-"+dataset);

		}

		// 2015
		String[] dataset_2015 = new String[]{
				"answers-forums.txt",
				"answers-students.txt",
				"belief.txt",
				"headlines.txt",
				"images.txt"
		};

		for (String dataset : dataset_2015) {
			String tempPath = "original/2015/test/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-"+dataset);

		}
		// 2016
		String[] dataset_2016 = new String[]{
				"answer-answer.txt",
				"headlines.txt",
				"plagiarism.txt",
				"postediting.txt",
				"question-question.txt"
		};

		stsInput = "STS2016.input.";
		stsGs = "STS2016.gs.";
		for (String dataset : dataset_2016) {
			String tempPath = "original/2016/test/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-" + dataset);

		}
*/

		// 2017
		String[] dataset_2017 = new String[]{
				"track5.en-en.txt",
		};

		stsInput = "STS.input.";
		stsGs = "STS.gs.";
		for (String dataset : dataset_2017) {
			String tempPath = "original/2017/test/";
			SentencePairWriter.write(path + tempPath + stsInput + dataset,
					path + tempPath + stsGs + dataset,
					resourcePath + tempPath + "Database-" + dataset);

		}

	}

	public static void write(String input, String answer, String newFile) throws IOException, InterruptedException, ExecutionException {

		if (nlpTools == null)
			nlpTools = new NLPTools();
		
		NUM_OF_THREADS = Runtime.getRuntime().availableProcessors();

		ExecutorService executorService = Executors.newFixedThreadPool(NUM_OF_THREADS + 2);
		Producer producer = new Producer(input, answer);

		producer.counter = new AtomicInteger(0);
		Future<?> submit = executorService.submit(producer);
		Writer writer = new Writer(newFile);
		executorService.execute(writer);
		List<Consumer> consumers = new ArrayList<>(NUM_OF_THREADS);
		for (int i = 0; i < NUM_OF_THREADS; i++) {
			Consumer consumer = new Consumer(producer, writer, nlpTools);
			consumers.add(consumer);
			executorService.execute(consumer);
		}

		for (Consumer consumer : consumers) {
			writer.addConsumer(consumer);
		}

		submit.get();
		
		System.out.println("100%");
		executorService.shutdown();
	}

	private static class Producer implements Runnable {

		private final String input;
		private final String answer;
		public AtomicInteger counter;
		public int total;
		public final BlockingQueue<StringPair> queue = new ArrayBlockingQueue<>(NUM_OF_THREADS * 10);
		public boolean finished = false;

		public Producer(String input, String answer) {
			this.input = input;
			this.answer = answer;
		}

		@Override
		public void run() {
			finished = false;
			try {
				String inputLine, classLine;
				BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(input)), "UTF8"));
				File fileAnswer = new File(answer);
				BufferedReader answerReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileAnswer), "UTF8"));
				total = countLines(fileAnswer);

				long start = System.currentTimeMillis();
				while ((inputLine = inputReader.readLine()) != null) {
					classLine = answerReader.readLine();

					if (classLine != null && !classLine.equals(""))
						queue.put(new StringPair(inputLine, classLine));

					long current = System.currentTimeMillis();
					float progress = (float) counter.get() * 100 / total;
					System.out.print("\r" + String.format("%.3f", progress) + "%");
					System.out.print(" (Elapsed : " + ((current - start) / 60000) + " mins )");

				}
				long finish = System.currentTimeMillis();

				System.out.println();
				System.out.println("Producer has finished its job. it took " + ((finish - start) / 60000.0) + " mins");

				inputReader.close();
				answerReader.close();

			} catch (Exception e) {
				System.out.println("Exception " + e.getMessage());
			}
			finished = true;
		}

		public static int countLines(File aFile) throws IOException {
			LineNumberReader reader = null;
			try {
				reader = new LineNumberReader(new FileReader(aFile));
				while ((reader.readLine()) != null) ;
				return reader.getLineNumber();
			} catch (Exception ex) {
				System.out.println("Exception " + ex.getMessage());
				return -1;
			} finally {
				if (reader != null)
					reader.close();
			}
		}
	}

	private static class Consumer implements Runnable {

		private final Producer producer;
		private final Writer writer;
		private final NLPTools nlpTools;
		public boolean finished = false;
		// BabelNet babelNet = new BabelNet();

		private Consumer(Producer producer, Writer writer, NLPTools nlpTools) {
			this.producer = producer;
			this.writer = writer;
			this.nlpTools = nlpTools;
		}

		@Override
		public void run() {
			StringPair pair = producer.queue.poll();
			while (!producer.finished || pair != null) {
				if (pair != null) {
					try {
						String[] sentences = nlpTools.toSentences(pair.getItem_1());

						List<Term> terms_1 = new ArrayList<>();

						List<CoreLabel> sen1_coreLabels = nlpTools.toCoreLabels(sentences[0]);
						for (CoreLabel sen1_coreLabel : sen1_coreLabels) {
							String lemma = sen1_coreLabel.get(CoreAnnotations.LemmaAnnotation.class);
							String pos = sen1_coreLabel.get(CoreAnnotations.PartOfSpeechAnnotation.class);
							Set<String> syns = nlpTools.getSynset(lemma, pos);
							int sentiment = nlpTools.findSentiment(lemma);
							// syns.addAll(babelNet.toSynsets(lemma));
							terms_1.add(new Term(sen1_coreLabel.word(),
									lemma,
									pos,
									syns,
									sentiment));
						}

						List<Term> terms_2 = new ArrayList<>();
						List<CoreLabel> sen2_coreLabels = nlpTools.toCoreLabels(sentences[1]);
						for (CoreLabel sen2_coreLabel : sen2_coreLabels) {
							String lemma = sen2_coreLabel.get(CoreAnnotations.LemmaAnnotation.class);
							String pos = sen2_coreLabel.get(CoreAnnotations.PartOfSpeechAnnotation.class);
							Set<String> syns = nlpTools.getSynset(lemma, pos);
							// syns.addAll(babelNet.toSynsets(lemma));
							int sentiment = nlpTools.findSentiment(lemma);
							terms_2.add(new Term(sen2_coreLabel.word(),
									lemma,
									pos,
									syns,
									sentiment));
						}

						Sentence sen1 = new Sentence(sentences[0], terms_1, (short) nlpTools.findSentiment(sentences[0]), nlpTools.nerTokens(sentences[0]));
						Sentence sen2 = new Sentence(sentences[1], terms_2, (short) nlpTools.findSentiment(sentences[1]), nlpTools.nerTokens(sentences[1]));
						SentencePair sentencePair = new SentencePair(sen1, sen2, Float.valueOf(pair.getItem_2()));
						writer.sentencePairs.put(sentencePair);
					} catch (Exception e) {
						e.printStackTrace();
					}
					producer.counter.incrementAndGet();
				}
				pair = producer.queue.poll();
			}
			finished = true;
		}
	}

	private static class Writer implements Runnable {

		public BlockingQueue<SentencePair> sentencePairs = new ArrayBlockingQueue<SentencePair>(100);
		private List<Consumer> consumers = new ArrayList<>();
		private final String path;

		private Writer(String path) {
			this.path = path;
		}

		@Override
		public void run() {
			try {
				File file = new File(path);
				// System.out.println("Writer has started its job, path : " + path);
				file.createNewFile();

				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				boolean finished = false;

				SentencePair pair = sentencePairs.poll();
				while (!finished || pair != null) {

					if (consumers.size() != 0)
						finished = true;

					for (int i = 0; i < consumers.size(); i++) {
						finished = finished && consumers.get(i).finished;
					}

					if (pair != null) {
						bw.write(pair.toJson() + System.lineSeparator());
					}
					
					pair = sentencePairs.poll();
					bw.flush();
				}
				bw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		public void addConsumer(Consumer consumer) {
			consumers.add(consumer);
		}
	}
}
