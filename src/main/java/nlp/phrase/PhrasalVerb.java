package nlp.phrase;

import java.util.List;
import java.util.Set;

public class PhrasalVerb {

    private String phraseVerb;
    private List<String> parts;
    private Set<String> synonyms;
    private boolean inseparable;

    public PhrasalVerb(String phraseVerb, List<String> parts, Set<String> synonyms, boolean inseparable) {
        this.phraseVerb = phraseVerb;
        this.parts = parts;
        this.synonyms = synonyms;
        this.inseparable = inseparable;
    }

    public String getPhraseVerb() {
        return phraseVerb;
    }

    public void setPhraseVerb(String phraseVerb) {
        this.phraseVerb = phraseVerb;
    }

    public List<String> getParts() {
        return parts;
    }

    public void setParts(List<String> parts) {
        this.parts = parts;
    }

    public boolean isInseparable() {
        return inseparable;
    }

    public void setInseparable(boolean separable) {
        this.inseparable = separable;
    }

    public Set<String> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(Set<String> synonyms) {
        this.synonyms = synonyms;
    }
}