package ml.dataset;

import ml.dataset.feature.FeatureAttribute;

import java.util.List;

public class DataSet {

    private final String name;
    private final List<FeatureAttribute> featureAttributes;
    private final FeatureTable featureTable;

    public DataSet(String name, FeatureTable featureTable, List<FeatureAttribute> featureAttributes) {
        this.name = name;
        this.featureAttributes = featureAttributes;
        this.featureTable = featureTable;
    }

    public String getName() {
        return name;
    }

    public List<FeatureAttribute> getFeatureAttributes() {
        return featureAttributes;
    }

    public FeatureTable getTrainingData() {
        return featureTable;
    }
}
