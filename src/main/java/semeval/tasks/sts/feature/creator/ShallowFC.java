package semeval.tasks.sts.feature.creator;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.ShortValue;
import semeval.tasks.sts.db.SentencePair;

import java.util.ArrayList;
import java.util.List;

public class ShallowFC implements FeatureCreator {

    @Override
    public List<FeatureValue> create(String line) {
        List<FeatureValue> features = new ArrayList<>(6);

        SentencePair sentencePair = SentencePair.toSentencePair(line);
        String sen1 = sentencePair.getSentence_1().getText();
        String sen2 = sentencePair.getSentence_2().getText();

        features.add(new ShortValue( Math.abs(sen1.length() - sen2.length())));
        features.add(new ShortValue(sen1.length()));
        features.add(new ShortValue(sen2.length()));

        features.add(new ShortValue(Math.abs(sentencePair.getSentence_1().getTerms().size() - sentencePair.getSentence_2().getTerms().size())));
        features.add(new ShortValue(sentencePair.getSentence_1().getTerms().size()));
        features.add(new ShortValue(sentencePair.getSentence_2().getTerms().size()));
        return features;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List ret = new ArrayList<>();
        ret.add(new FeatureAttribute("SentenceLengthDifference"    , FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("Sentence_1_Length"       , FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("Sentence_2_Length"       , FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("WordCountDifference", FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("Sentence_1_WordCount"   , FeatureAttribute.AttributeType.NUMERIC));
        ret.add(new FeatureAttribute("Sentence_2_WordCount"   , FeatureAttribute.AttributeType.NUMERIC));
        return ret;
    }
}
