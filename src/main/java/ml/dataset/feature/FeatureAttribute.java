package ml.dataset.feature;

import java.util.List;

public class FeatureAttribute {

    public enum AttributeType {NUMERIC, STRING}

    private final String name;
    private List<String> possibleValues;
    private AttributeType attributeType;

    public FeatureAttribute(String name, List<String> possibleValues) {
        this.name = name;
        this.possibleValues = possibleValues;
    }

    public FeatureAttribute(String name, AttributeType attributeType) {
        this.name = name;
        this.attributeType = attributeType;
    }

    public String getName() {
        return name;
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }

    public AttributeType getAttributeType() {
        return attributeType;
    }
}
