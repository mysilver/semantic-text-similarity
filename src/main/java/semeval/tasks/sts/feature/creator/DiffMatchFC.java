package semeval.tasks.sts.feature.creator;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohammad-Ali on 11/04/2016.
 */
public class DiffMatchFC implements FeatureCreator {


    private float THRESHOLD = .7f;


    @Override
    public List<FeatureValue> create(String line) {
        SentencePair sentencePair = SentencePair.toSentencePair(line);

        List<Term> terms_1 = sentencePair.getSentence_1().getTerms();
        List<Term> terms_2 = sentencePair.getSentence_2().getTerms();

        List<String> lemmas_1 = sentencePair.getSentence_1().getLemmas();
        List<String> lemmas_2 = sentencePair.getSentence_2().getLemmas();

        List<Term> remainingFrom_Sen1 = new ArrayList<>();
        List<Term> remainingFrom_Sen2 = new ArrayList<>();


        for (Term term1 : terms_1) {
            for (String syn : term1.getSynonyms()) {
                boolean noMatch = true;
                for (Term term2 : terms_2) {
                    for (String syn2 : term2.getSynonyms()) {
                        if(syn.equals(syn2)) {
                            noMatch = false;
                            break;
                        }
                    }
                }
                if (noMatch)
                    remainingFrom_Sen1.add(term1);
            }


        }

        float match = 0;

        List<FeatureValue> ret = new ArrayList<>();
        float coverage = (float)remainingFrom_Sen1.size() / terms_1.size();
        if( coverage > THRESHOLD)
        {
            ret.add(new FloatValue(Math.log(coverage + 0.001)));
        }
        else
            ret.add(new FloatValue(0));



        return ret;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List<FeatureAttribute>  featureAttributes = new ArrayList<>(1);
        featureAttributes.add(new FeatureAttribute("DiffMatch", FeatureAttribute.AttributeType.NUMERIC));
        return featureAttributes;
    }
}
