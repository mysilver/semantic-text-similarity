package ml.dataset.feature;

import ml.dataset.feature.value.FeatureValue;

import java.util.List;

public interface FeatureCreator {

    List<FeatureValue> create(String line);
    List<FeatureAttribute> attributes();
}
