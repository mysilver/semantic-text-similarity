package evaluation;
import edu.stanford.nlp.util.Pair;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import semeval.tasks.sts.db.SentencePair;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.trees.M5P;
import weka.core.Instances;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.function.Consumer;

public class WekaEvaluation {

	LinearRegression classifier = new LinearRegression();
//	M5P classifier = new M5P();
	Instances trainingInstances;
	private double coefficient;
	private double numInstances;

	public WekaEvaluation(String trainDatasetPath) throws Exception {
		trainingInstances = new Instances(readDataFile(trainDatasetPath));
		trainingInstances.setClassIndex(trainingInstances.numAttributes() - 1);
		classifier.setRidge(7000);
		classifier.buildClassifier(trainingInstances);
		weka.core.SerializationHelper.write("/home/may/projects/semantic-text-similarity/files/sts_model.weka", classifier);
	}

	// Classifier classifier = new SMOreg();
	public double[] evaluate(String testDatasetPath) throws Exception {
		Evaluation evaluation = new Evaluation(trainingInstances);
		Instances testInstances = new Instances(readDataFile(testDatasetPath));
		testInstances.setClassIndex(testInstances.numAttributes() - 1);
		double[] doubles = evaluation.evaluateModel(classifier, testInstances);
		coefficient = evaluation.correlationCoefficient();
		numInstances = evaluation.numInstances();
		print(evaluation);
		return doubles;
	}

	private static void print(Evaluation evaluation) throws Exception {
		System.out.println(evaluation.toSummaryString());
	}

	public static BufferedReader readDataFile(String filename) {
		BufferedReader inputReader = null;

		try {
			inputReader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException ex) {
			System.err.println("File not found: " + filename);
		}

		return inputReader;
	}

	private static final Comparator<Pair<SentencePair, Float>> comparatorWorse = (unit1, unit2) ->
			(int)((Math.abs(unit2.first().getClassName() - unit2.second())
					- (Math.abs(unit1.first().getClassName() - unit1.second()))) * 10000);

	private static final Comparator<Pair<SentencePair, Float>> comparatorBest = (unit1, unit2) ->
			(int)((Math.abs(unit1.first().getClassName() - unit1.second())
					- (Math.abs(unit2.first().getClassName() - unit2.second()))) * 10000);

	public static String explains(boolean worseCases, String[] testDatabasePaths, double[] predictions, int top, List<FeatureCreator> featureCreators) throws IOException {

		StringBuilder stringBuilder = new StringBuilder();
		PriorityQueue<Pair<SentencePair, Float>>[] sentencePairsQueues = new PriorityQueue[6];
		for (int i = 0; i < 6; i++)
			sentencePairsQueues[i] = new PriorityQueue<>(top, worseCases ? comparatorWorse : comparatorBest);

		String inputLine;
		int lineNumber = -1;
		for (String testDatabasePath : testDatabasePaths) {
			File dbFile = new File(testDatabasePath);
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(dbFile), "UTF8"));

			while ((inputLine = inputReader.readLine()) != null) {
				lineNumber++;
				if (inputLine.trim().equals(""))
					continue;
				SentencePair sentencePair = SentencePair.toSentencePair(inputLine);
				sentencePairsQueues[Math.min(Math.round(sentencePair.getClassName()), 5) ].add(new Pair<>(sentencePair, (float) predictions[lineNumber]));
			}
			inputReader.close();
		}

		for (PriorityQueue<Pair<SentencePair, Float>> pairPriorityQueue : sentencePairsQueues) {
			Pair<SentencePair, Float> current;
			int counter = 0;
			while ((current = pairPriorityQueue.poll())!=null && counter++ < top){
				stringBuilder.append("===============================================================================\n");
				stringBuilder.append("Prediction:\t").append(current.second()).append("\n");
				stringBuilder.append(current.first()).append("\n");
				for (FeatureCreator featureCreator : featureCreators) {
					List<FeatureValue> featureValues = featureCreator.create(current.first().toJson());
					featureValues.forEach(fv -> stringBuilder.append(featureCreator.getClass().getSimpleName()).append(" = ").append(fv.value()).append("\t"));
				}
				stringBuilder.append("\n");

			}
		}

		return stringBuilder.toString();
	}

	public double getCoefficient() {
		return coefficient;
	}

	public double getNumInstances() {
		return numInstances;
	}
}
