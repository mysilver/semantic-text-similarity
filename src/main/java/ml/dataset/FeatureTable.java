package ml.dataset;

import ml.dataset.feature.FeatureVector;

import java.util.ArrayList;
import java.util.List;

public class FeatureTable {

    private List<FeatureVector> featureVectors;

    public FeatureTable() {
        this.featureVectors = new ArrayList<>();
    }

    public List<FeatureVector> getFeatureVector() {
        return featureVectors;
    }

    public FeatureTable add(FeatureVector featureVector) {
        this.featureVectors.add(featureVector);
        return this;
    }

}
