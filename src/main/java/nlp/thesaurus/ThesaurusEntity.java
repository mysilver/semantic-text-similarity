package nlp.thesaurus;

import nlp.PartsOfSpeech;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohammad-Ali on 12/05/2016.
 */
public class ThesaurusEntity {

    private String entity;
    private List<ThesaurusSense> wordNet;
    private List<ThesaurusSense> collins;
    private List<ThesaurusSense> rogets;

    public ThesaurusEntity(String entity) {
        this.entity = entity;
        this.wordNet = new ArrayList<>(3);
        this.collins = new ArrayList<>(3);
        this.rogets = new ArrayList<>(3);
    }

    public void addWordNetSense(ThesaurusSense sense) {
        wordNet.add(sense);
    }

    public void addWordNetSense(List<ThesaurusSense> sense) {
        wordNet.addAll(sense);
    }

    public void addCollinsSense(ThesaurusSense sense) {
        collins.add(sense);
    }

    public void addCollinsSense(List<ThesaurusSense> sense) {
        collins.addAll(sense);
    }

    public void addRogetsSense(ThesaurusSense sense) {
        rogets.add(sense);
    }

    public void addRogetsSense(List<ThesaurusSense> sense) {
        rogets.addAll(sense);
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public List<ThesaurusSense> getWordNet() {
        return wordNet;
    }

    public void setWordNet(List<ThesaurusSense> wordNet) {
        this.wordNet = wordNet;
    }

    public List<ThesaurusSense> getCollins() {
        return collins;
    }

    public void setCollins(List<ThesaurusSense> collins) {
        this.collins = collins;
    }

    public List<ThesaurusSense> getRogets() {
        return rogets;
    }

    public void setRogets(List<ThesaurusSense> rogets) {
        this.rogets = rogets;
    }

    public boolean hasEqualSenseWith(ThesaurusEntity entity, PartsOfSpeech pos) {
        return hasEqualSenseWith(entity, pos, false);
    }

    public boolean hasEqualSenseWith(ThesaurusEntity entity, PartsOfSpeech pos, boolean L2) {

        if (entity == null)
            return false;

        if (this.entity.equalsIgnoreCase(entity.entity))
            return true;

        if (!L2)
            return assessEquality_L1(entity.getWordNet()) ||
                    assessEquality_L1(entity.getCollins()) ||
                    assessEquality_L1(entity.getRogets());

        return assessEquality_L2(this.getWordNet(), entity.getWordNet(), pos) ||
                assessEquality_L2(this.getCollins(), entity.getCollins(), pos) ||
                assessEquality_L2(this.getRogets(), entity.getRogets(), pos);
    }

    private boolean assessEquality_L1(List<ThesaurusSense> otherSenses) {
        for (ThesaurusSense sense : otherSenses)
                if (sense.getSynonyms().contains(this.entity))
                    return true;
        return false;
    }

    private boolean assessEquality_L2(List<ThesaurusSense> senses, List<ThesaurusSense> otherSenses, PartsOfSpeech pos) {

        for (ThesaurusSense thesaurusSense1 : senses) {
            if (PartsOfSpeech.thesaurusToPartsOfSpeech(thesaurusSense1.getPos()) == pos)
                for (ThesaurusSense thesaurusSense2 : otherSenses) {
                    if (PartsOfSpeech.thesaurusToPartsOfSpeech(thesaurusSense2.getPos()) == pos) {
                        if (thesaurusSense1.getSense().equalsIgnoreCase(thesaurusSense2.getSense()))
                            return true;
                        for (String sense1 : thesaurusSense1.getSynonyms()) {
                            for (String sense2 : thesaurusSense2.getSynonyms()) {
                                if (sense1.equalsIgnoreCase(sense2))
                                    return true;
                            }
                        }
                    }
                }
        }
        return false;
    }

    public boolean isRelatedTo(ThesaurusEntity entity, PartsOfSpeech pos) {

        return entity != null &&
                (this.entity.equalsIgnoreCase(entity.entity)
                        || assessRelevance(this.getWordNet(), entity.getWordNet(), pos)
                        || assessRelevance(this.getCollins(), entity.getCollins(), pos)
                        || assessRelevance(this.getRogets(), entity.getRogets(), pos));

    }

    private boolean assessRelevance(List<ThesaurusSense> senses, List<ThesaurusSense> otherSenses, PartsOfSpeech pos) {

        for (ThesaurusSense thesaurusSense1 : senses) {
            if (PartsOfSpeech.thesaurusToPartsOfSpeech(thesaurusSense1.getPos()) == pos)
                for (ThesaurusSense thesaurusSense2 : otherSenses) {
                    if (!thesaurusSense1.getPos().equalsIgnoreCase(thesaurusSense2.getPos()))
                        continue;

                    if (thesaurusSense1.getSense().equalsIgnoreCase(thesaurusSense2.getSense()))
                        return true;
                    for (String sense1 : thesaurusSense1.getRelatedWords()) {
                        if (thesaurusSense2.getSense().equalsIgnoreCase(sense1))
                            return true;
                        for (String sense2 : thesaurusSense2.getRelatedWords()) {
                            if (thesaurusSense1.getSense().equalsIgnoreCase(sense2))
                                return true;
                            if (sense1.equalsIgnoreCase(sense2))
                                return true;
                        }
                    }

                }
        }

        return false;
    }

    public boolean isAntonymOf(ThesaurusEntity entity) {
        return entity != null
                && !this.entity.equalsIgnoreCase(entity.entity)
                && (assessAntonym(entity.getWordNet())
                || assessAntonym(entity.getCollins())
                || assessAntonym(entity.getRogets()));

    }

    private boolean assessAntonym(List<ThesaurusSense> senses) {

        for (ThesaurusSense sense : senses) {
            if (sense.getAntonyms() != null)
                if (sense.getAntonyms().contains(this.entity.toLowerCase()))
                    return true;
        }
        return false;
    }
}
