package semeval.tasks.sts.feature.creator.dkpro.simulated.lexical;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OverlapCoefficient implements FeatureCreator{
	@Override
	public List<FeatureValue> create(String line) {

		SentencePair sentencePair = SentencePair.toSentencePair(line);

		final Set<String> allTokens = new HashSet<String>();
		allTokens.addAll(sentencePair.getSentence_1().getLemmas());
		final int termsInString1 = allTokens.size();
		final Set<String> secondStringTokens = new HashSet<String>();
		secondStringTokens.addAll(sentencePair.getSentence_2().getLemmas());
		final int termsInString2 = secondStringTokens.size();

		//now combine the sets
		allTokens.addAll(secondStringTokens);
		final int commonTerms = (termsInString1 + termsInString2) - allTokens.size();

		//return overlap_coefficient

		List<FeatureValue> ret = new ArrayList<>();
		ret.add(new FloatValue((commonTerms) / (float) Math.min(termsInString1, termsInString2)));

		return ret;
	}

	@Override
	public List<FeatureAttribute> attributes() {
		List<FeatureAttribute> ret = new ArrayList<>(1);
		ret.add(new FeatureAttribute("OverlapCoefficient", FeatureAttribute.AttributeType.NUMERIC));
		return ret;
	}
}
