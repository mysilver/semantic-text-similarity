package nlp.synonyms;

import nlp.synonyms.number.NumberToWordConverter;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.db.Term;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by may on 4/26/16.
 */
public class SynsetBuilder {

	static Map<String, Set<String>> Synset = new HashMap<>(9999999);

	public static void main(String[] args) {
		String resourcePath = SynsetBuilder.class.getClassLoader().getResource("").getFile().replace("target/classes", "src/main/resources/dataset");

		// 2012
		String[] dataset_test_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt",
				"surprise.OnWN.txt",
				"surprise.SMTnews.txt"
		};

		for (String dataset : dataset_test_2012) {
			String tempPath = "original/2012/test-gold/";
			readFile(resourcePath + tempPath + "Database-" + dataset);
		}

		String[] dataset_train_2012 = new String[]{
				"MSRpar.txt",
				"MSRvid.txt",
				"SMTeuroparl.txt"
		};

		for (String dataset : dataset_train_2012) {
			String tempPath = "original/2012/train/";
			readFile(resourcePath + tempPath + "Database-" + dataset);readFile(resourcePath + tempPath + "Database-"+dataset);

		}

		// 2013
		String[] dataset_2013 = new String[]{
				"FNWN.txt",
				"headlines.txt",
				"OnWN.txt"
		};

		for (String dataset : dataset_2013) {
			String tempPath = "original/2013/test-gs/";
			readFile(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2014
		String[] dataset_2014 = new String[]{
				"deft-forum.txt",
				"deft-news.txt",
				"headlines.txt",
				"images.txt",
				"OnWN.txt",
				"tweet-news.txt"
		};

		for (String dataset : dataset_2014) {
			String tempPath = "original/2014/test/";
			readFile(resourcePath + tempPath + "Database-" + dataset);

		}

		// 2015
		String[] dataset_2015 = new String[]{
				"answers-forums.txt",
				"answers-students.txt",
				"belief.txt",
				"headlines.txt",
				"images.txt"
		};

		for (String dataset : dataset_2015) {
			String tempPath = "original/2015/test/";
			readFile(resourcePath + tempPath + "Database-" + dataset);

		}
		// 2016
		String[] dataset_2016 = new String[]{
				"answer-answer.txt",
				"headlines.txt",
				"plagiarism.txt",
				"postediting.txt",
				"question-question.txt"
		};

		for (String dataset : dataset_2016) {
			String tempPath = "original/2016/test/";
			readFile(resourcePath + tempPath + "Database-" + dataset);

		}

		writeFile("D:/synset.txt");

	}

	public static void readFile(String path) {
		try {
			String inputLine;
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));

			while ((inputLine = inputReader.readLine()) != null) {
				SentencePair sentencePair = SentencePair.toSentencePair(inputLine);

				for (Term term : sentencePair.getSentence_1().getTerms()) {
					if (!Synset.containsKey(term.getLemma())) {
						Set<String> synonyms = term.getSynonyms();
						if (term.getPos().equals("CD")) {
							try {
								synonyms.add(NumberToWordConverter.convert(Integer.valueOf(term.getWord())));
							}catch (Exception e){
							}
						}
						Synset.put(term.getLemma(), synonyms);
					}
				}
				for (Term term : sentencePair.getSentence_2().getTerms()) {
					if (!Synset.containsKey(term.getLemma())) {
						Set<String> synonyms = term.getSynonyms();
						if (term.getPos().equals("CD")) {
							try {
								synonyms.add(NumberToWordConverter.convert(Integer.valueOf(term.getWord())));
							}catch (Exception e){
							}
						}
						Synset.put(term.getLemma(), synonyms);
					}
				}
			}

			inputReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeFile(String path) {
		try {
			File file = new File(path);
			// System.out.println("Writer has started its job, path : " + path);
			file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);

			for (Map.Entry<String, Set<String>> wordSynset : Synset.entrySet()) {
				bw.write(wordSynset.getKey() + "\t" + commas(wordSynset.getValue()) + System.lineSeparator());
				bw.flush();
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static String commas(Set<String> value) {
		StringBuilder sb = new StringBuilder();
		for (String s : value) {
			sb.append(s).append(',');
		}
		if (sb.length() > 0)
			return sb.substring(0,sb.length()-1);

		return sb.toString();
	}

	static Pattern splitterTab = Pattern.compile("\t");
	static Pattern splitterComma = Pattern.compile(",");

	public static Map<String, Set<String>> toSynset(String path) {
		Map<String, Set<String>> synset = new HashMap<>();
		try {
			String inputLine;
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), "UTF8"));

			while ((inputLine = inputReader.readLine()) != null) {
				String[] split = splitterTab.split(inputLine);
				if (split.length < 2)
					continue;

				String[] syns = splitterComma.split(split[1]);

				Set<String> words = new HashSet<>();
				for (String syn : syns) {
					words.add(syn);
				}

				synset.put(split[0], words);
			}

			inputReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return synset;
	}
}
