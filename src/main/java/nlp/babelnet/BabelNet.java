/*
package nlp.babelnet;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.BabelSense;
import it.uniroma1.lcl.babelnet.BabelSenseSource;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.jlt.util.Language;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class BabelNet {

    ThreadLocal<it.uniroma1.lcl.babelnet.BabelNet> babelNetThreadLocal = new InheritableThreadLocal<it.uniroma1.lcl.babelnet.BabelNet>() {
        @Override
        protected it.uniroma1.lcl.babelnet.BabelNet initialValue() {
            return it.uniroma1.lcl.babelnet.BabelNet.getInstance();
        }
    };

    LoadingCache<String, Set<String>> sysnCache = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(10, TimeUnit.HOURS)
            .build(new CacheLoader<String, Set<String>>() {
                       @Override
                       public Set<String> load(String lemma) throws Exception {
                           Set<String> synsetLemams = new HashSet<>();
                           for (POS pos : POS.values()) {
                               synsetLemams.addAll(toSynsets(lemma, pos));
                           }
                           return synsetLemams;
                       }
                   }
            );

    public Set<String> toSynsets(String lemma) throws IOException {
        try {
            return sysnCache.get(lemma);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return new HashSet<>(0);
        }
    }

    public Set<String> toSynsets(String lemma, POS pos) throws IOException {

        List synsets1 = babelNetThreadLocal.get().getSynsets(Language.EN, lemma, pos, true, new BabelSenseSource[0]);
        //Collections.sort(synsets1, new BabelSynsetComparator(lemma));
        Iterator var8 = synsets1.iterator();

        Set<String> synsetLemams = new HashSet<>();
        while (var8.hasNext()) {
            BabelSynset synset1 = (BabelSynset) var8.next();
            for (BabelSense babelSense : synset1.getSenses(Language.EN)) {
                String term = babelSense.getLemma().toLowerCase();
                if (!term.contains("_") && !term.contains("-") && !term.contains("(") && !term.contains("."))
                    synsetLemams.add(term);
            }
        }
        return synsetLemams;
    }

    public static void main(String[] args) throws IOException {

        String lemma = "yell";

        BabelNet babelNet = new BabelNet();
        Set<String> synsetLemams = babelNet.toSynsets(lemma);

        synsetLemams.forEach(System.out::println);

    }
}
*/
