package misc;

/**
 * Created by mohammad-ali on 4/6/16.
 */
public class StringPair {
	
	private final String item_1;
	private final String item_2;

	public StringPair(String item_1, String item_2) {
		this.item_1 = item_1;
		this.item_2 = item_2;
	}

	public String getItem_1() {
		return item_1;
	}

	public String getItem_2() {
		return item_2;
	}
}
