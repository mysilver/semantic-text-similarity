package evaluation;

import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.BooleanValue;
import ml.dataset.feature.value.FeatureValue;
import semeval.tasks.sts.feature.creator.NerFC;
import semeval.tasks.sts.feature.creator.NounVerbMatchingFC;
import semeval.tasks.sts.feature.creator.SentimentFC;
import semeval.tasks.sts.feature.creator.SynVectorFC;
import semeval.tasks.sts.feature.creator.dkpro.LexicalFeatureCreator;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.WordNGramsFeatureCreator;
import semeval.tasks.sts.feature.creator.dkpro.simulated.structure.POSNGramsFeatureCreator;
import weka.classifiers.functions.LinearRegression;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mohammad-Ali on 8/04/2016.
 */
public class TestPrediction {

    public static void main( String... args) throws Exception {

        LinearRegression classifier = new LinearRegression();
        Instances trainingInstances = new Instances(readDataFile("/home/may/yooz-projects/semantic-text-similarity3/target/classes/dataset/TrainingWekaFile.arff"));
        trainingInstances.setClassIndex(trainingInstances.numAttributes() - 1);
        classifier.buildClassifier(trainingInstances);

        String line = "{\"Sentence_1\":{\"text\":\"The methodology takes much less time rather than naive methods.\",\"terms\":[{\"word\":\"The\",\"lemma\":\"the\",\"pos\":\"DT\",\"synonyms\":[]},{\"word\":\"methodology\",\"lemma\":\"methodology\",\"pos\":\"NN\",\"synonyms\":[\"methodological_analysis\",\"methodology\"]},{\"word\":\"takes\",\"lemma\":\"take\",\"pos\":\"VBZ\",\"synonyms\":[\"take\"]},{\"word\":\"much\",\"lemma\":\"much\",\"pos\":\"RB\",\"synonyms\":[\"much\"]},{\"word\":\"less\",\"lemma\":\"less\",\"pos\":\"JJR\",\"synonyms\":[\"less\"]},{\"word\":\"time\",\"lemma\":\"time\",\"pos\":\"NN\",\"synonyms\":[\"time\",\"clip\"]},{\"word\":\"rather\",\"lemma\":\"rather\",\"pos\":\"RB\",\"synonyms\":[\"rather\",\"instead\"]},{\"word\":\"than\",\"lemma\":\"than\",\"pos\":\"IN\",\"synonyms\":[]},{\"word\":\"naive\",\"lemma\":\"naive\",\"pos\":\"JJ\",\"synonyms\":[\"naif\",\"naive\"]},{\"word\":\"methods\",\"lemma\":\"method\",\"pos\":\"NNS\",\"synonyms\":[\"method\"]},{\"word\":\".\",\"lemma\":\".\",\"pos\":\".\",\"synonyms\":[]}],\"sentiment\":1,\"nameEntities\":[]},\"Sentence_2\":{\"text\":\"The method is more effiecent than naive methods.\",\"terms\":[{\"word\":\"The\",\"lemma\":\"the\",\"pos\":\"DT\",\"synonyms\":[]},{\"word\":\"method\",\"lemma\":\"method\",\"pos\":\"NN\",\"synonyms\":[\"method\"]},{\"word\":\"is\",\"lemma\":\"be\",\"pos\":\"VBZ\",\"synonyms\":[\"be\"]},{\"word\":\"more\",\"lemma\":\"more\",\"pos\":\"RBR\",\"synonyms\":[\"more\",\"to_a_greater_extent\"]},{\"word\":\"effiecent\",\"lemma\":\"effiecent\",\"pos\":\"JJ\",\"synonyms\":[\"effiecent\"]},{\"word\":\"than\",\"lemma\":\"than\",\"pos\":\"IN\",\"synonyms\":[]},{\"word\":\"naive\",\"lemma\":\"naive\",\"pos\":\"JJ\",\"synonyms\":[\"naif\",\"naive\"]},{\"word\":\"methods\",\"lemma\":\"method\",\"pos\":\"NNS\",\"synonyms\":[\"method\"]},{\"word\":\".\",\"lemma\":\".\",\"pos\":\".\",\"synonyms\":[]}],\"sentiment\":2,\"nameEntities\":[]},\"className\":5.0}\n";
        List<FeatureCreator> featureCreators = new ArrayList<>();

        featureCreators.add(new LexicalFeatureCreator());

        featureCreators.add(new WordNGramsFeatureCreator(3));
        featureCreators.add(new WordNGramsFeatureCreator(4));

        // POS Ngrams
        featureCreators.add(new POSNGramsFeatureCreator(2));
        featureCreators.add(new POSNGramsFeatureCreator(3));
        featureCreators.add(new POSNGramsFeatureCreator(4));

        /**
         * NOT USEFUL FEATURES
         * In the future things may change
         */
        featureCreators.add(new SynVectorFC());
        featureCreators.add(new SentimentFC());
        featureCreators.add(new NounVerbMatchingFC());
        featureCreators.add(new NerFC());

        List<FeatureValue> featureValues = new ArrayList<>();
        for (FeatureCreator featureCreator : featureCreators) {
            featureValues.addAll(featureCreator.create(line));
        }

        Instance instance = new DenseInstance(1, toDoubles(featureValues));
        System.out.println(classifier.classifyInstance(instance));


    }

    private static double[] toDoubles(List<FeatureValue> featureValues) {

        double [] ret= new double[featureValues.size() + 1];
        int index = 0;
        for (FeatureValue featureValue : featureValues) {

            if (featureValue instanceof BooleanValue)
                ret[index] = Boolean.valueOf(featureValue.value())? 1:0;
            else
                ret[index] = Double.valueOf(featureValue.value());
            index++;
        }
        ret[featureValues.size()-1] = new Random().nextInt();
        return ret;
    }

    public static BufferedReader readDataFile(String filename) {
        BufferedReader inputReader = null;

        try {
            inputReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException ex) {
            System.err.println("File not found: " + filename);
        }

        return inputReader;
    }
}
