package semeval.tasks.sts.feature.creator.dkpro;

import dkpro.similarity.algorithms.api.SimilarityException;
import dkpro.similarity.algorithms.api.TextSimilarityMeasure;
import dkpro.similarity.algorithms.lexical.string.*;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;
import semeval.tasks.sts.feature.creator.dkpro.simulated.lexical.CharacterNGramMeasure;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LexicalFeatureCreator implements FeatureCreator {

    List<TextSimilarityMeasure> measures = new ArrayList<>();
    List<TextSimilarityMeasure> measures_2 = new ArrayList<>();

    public LexicalFeatureCreator() throws SimilarityException {
        String idfDir = "charsNgrams";
        File f = new File(idfDir);
        if (!f.exists() || !f.isDirectory())
            idfDir = getClass().getClassLoader().getResource("").getFile() + "charsNgrams/";
        else
            idfDir = f.getPath() + "/";
        // ASL
        for (int i = 2; i < 5; i++) {
            try {
                measures.add(new CharacterNGramMeasure(i, idfDir + "idf_" + i + "ngrms"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i = 3; i < 5; i++) {
            //measures_2.add(new WordNGramJaccardMeasure(i)); SIMULATED
            //measures_2.add(new WordNGramContainmentMeasure(i)); SIMULATED
            measures.add(new GreedyStringTiling(i));
        }

        // measures.add(new ExactStringMatchComparator());
        // measures.add(new BoundedSubstringMatchComparator());
        // measures.add(new SubstringMatchComparator());

//        measures.add(new CosineSimilarity());

//        measures.add(new JaroWinklerSecondStringComparator());
//        measures.add(new JaroSecondStringComparator());

        measures.add(new LongestCommonSubsequenceComparator());
        measures.add(new LongestCommonSubsequenceNormComparator());
        measures.add(new LongestCommonSubstringComparator());


//        measures.add(new LevenshteinComparator());
//        measures.add(new LevenshteinSecondStringComparator());
        measures.add(new MongeElkanSecondStringComparator());

    }

    @Override
    public List<FeatureValue> create(String line) {
        List<FeatureValue> ret = new ArrayList<>();

        SentencePair sentencePair = SentencePair.toSentencePair(line);
        String lemma_1 = sentencePair.getSentence_1().getLemma();
        String lemma_2 = sentencePair.getSentence_2().getLemma();

        for (TextSimilarityMeasure measure : measures) {
            try {
                double score = measure.getSimilarity(lemma_1, lemma_2);
                ret.add(new FloatValue((float) score));
            } catch (Exception e) {
                System.out.println(measure);
                e.printStackTrace();
            }
        }

        for (TextSimilarityMeasure measure : measures_2) {
            try {
                double score = measure.getSimilarity(sentencePair.getSentence_1().getLemmas(), sentencePair.getSentence_2().getLemmas());
                ret.add(new FloatValue((float) score));
            } catch (Exception e) {
                System.out.println(measure);
                e.printStackTrace();
            }
        }

        return ret;
    }

    @Override
    public List<FeatureAttribute> attributes() {
        List<FeatureAttribute> ret = new ArrayList<>();
        for (TextSimilarityMeasure measure : measures) {
            ret.add(new FeatureAttribute(measure.getName(), FeatureAttribute.AttributeType.NUMERIC));
        }
        for (TextSimilarityMeasure measure : measures_2) {
            ret.add(new FeatureAttribute(measure.getName(), FeatureAttribute.AttributeType.NUMERIC));
        }
        return ret;
    }
}
