package semeval.tasks.sts.feature.creator.dkpro.simulated.lexical;

import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureCreator;
import ml.dataset.feature.value.FeatureValue;
import ml.dataset.feature.value.FloatValue;
import semeval.tasks.sts.db.SentencePair;

import java.io.IOException;
import java.util.*;

public class WordNGramsFeatureCreator implements FeatureCreator {

	private final int n;

	public WordNGramsFeatureCreator(int ngram) throws IOException {
		this.n = ngram;
	}

	@Override
	public List<FeatureValue> create(String line) {

		SentencePair sentencePair = SentencePair.toSentencePair(line);
		List<FeatureValue> ret = new ArrayList<>(1);
		ret.add(new FloatValue(getPosNGramSimilarity(sentencePair.getSentence_1().getLemmas(), sentencePair.getSentence_2().getLemmas())));
		ret.add(new FloatValue(getPosNGramSimilarity2(sentencePair.getSentence_1().getLemmas(), sentencePair.getSentence_2().getLemmas())));
		return ret;
	}

	@Override
	public List<FeatureAttribute> attributes() {
		List ret = new ArrayList<>(1);
		ret.add(new FeatureAttribute("WordNGramJaccardMeasure_" + n + "grams", FeatureAttribute.AttributeType.NUMERIC));
		ret.add(new FeatureAttribute("WordNGramContainmentMeasure_" + n + "grams", FeatureAttribute.AttributeType.NUMERIC));
		return ret;
	}

	private double getPosNGramSimilarity(Collection<String> pos1, Collection<String> pos2) {
		// Get n-grams
		Set<List<String>> ngrams1 = getPosNGrams(new ArrayList<String>(pos1));
		Set<List<String>> ngrams2 = getPosNGrams(new ArrayList<String>(pos2));

		double sim = getNormalizedSimilarity(ngrams1, ngrams2);

		return sim;
	}

	private Set<List<String>> getPosNGrams(List<String> pos)
	{
		Set<List<String>> ngrams = new HashSet<List<String>>();

		for (int i = 0; i < pos.size() - (n - 1); i++)
		{
			// Generate n-gram at index i
			List<String> ngram = new ArrayList<String>();
			for (int k = 0; k < n; k++)
			{
				String token = pos.get(i + k);
				ngram.add(token.toLowerCase());
			}

			// Add
			ngrams.add(ngram);
		}

		return ngrams;
	}

	protected double getNormalizedSimilarity(Set<List<String>> suspiciousNGrams, Set<List<String>> originalNGrams)
	{
		// Compare using the Jaccard similarity coefficient (Manning & Schütze, 1999)
		Set<List<String>> commonNGrams = new HashSet<List<String>>();
		commonNGrams.addAll(suspiciousNGrams);
		commonNGrams.retainAll(originalNGrams);

		Set<List<String>> unionNGrams = new HashSet<List<String>>();
		unionNGrams.addAll(suspiciousNGrams);
		unionNGrams.addAll(originalNGrams);

		double norm = unionNGrams.size();
		double sim = 0.0;

		if (norm > 0.0) {
			sim = commonNGrams.size() / norm;
		}

		return sim;
	}

	protected double getNormalizedSimilarity2(Set<List<String>> suspiciousNGrams, Set<List<String>> originalNGrams)
	{
		// Compare using the Containment measure (Broder, 1997)
		Set<List<String>> commonNGrams = new HashSet<List<String>>();
		commonNGrams.addAll(suspiciousNGrams);
		commonNGrams.retainAll(originalNGrams);

		double norm = suspiciousNGrams.size();
		double sim = 0.0;

		if (norm > 0.0)
			sim = commonNGrams.size() / norm;

		return sim;
	}

	private double getPosNGramSimilarity2(Collection<String> pos1, Collection<String> pos2) {
		// Get n-grams
		Set<List<String>> ngrams1 = getPosNGrams(new ArrayList<String>(pos1));
		Set<List<String>> ngrams2 = getPosNGrams(new ArrayList<String>(pos2));

		double sim = getNormalizedSimilarity2(ngrams1, ngrams2);

		return sim;
	}

}
