package nlp.phrase;

import edu.stanford.nlp.util.Pair;
import nlp.thesaurus.Thesaurus;
import semeval.tasks.sts.db.Sentence;
import semeval.tasks.sts.db.Term;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class PhrasalVerbs {

    private Map<String,PhrasalVerb> phrasalVerbs = new HashMap<>();

    public PhrasalVerbs() {

        String file = PhrasalVerbs.class.getResource("/phrasal-verbs.txt").getPath();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine())!=null) {
                String[] split = line.toLowerCase().trim().split("\t");
                String[] phrases = split[0].toLowerCase().trim().split(" ");
                String[] syns = split[2].toLowerCase().trim().split(",");
                phrasalVerbs.put(split[0],
                        new PhrasalVerb(split[0],Arrays.asList(phrases),
                                new HashSet<>(Arrays.asList(syns)),
                                Boolean.valueOf(split[1])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.err.println("phrasal-verb list is ready");
    }

    Set<String> availblePhrases = Thesaurus.load().getPhrases().keySet();
    public Sentence replacePhrasalVerbs(Sentence sentence) {

        // VerbIndex, <Verb, RestIndex>
        Map<Integer, Pair<List<String>, Integer>> candidates = new HashMap<>();

        for (PhrasalVerb phrasalVerb : phrasalVerbs.values()) {

            if (!availblePhrases.contains(phrasalVerb.getPhraseVerb()))
                continue;

            if (phrasalVerb.getParts().size() < 2)
                continue;

            if (phrasalVerb.isInseparable() && !sentence.getLemma().contains(phrasalVerb.getPhraseVerb()))
                continue;

            String verb = phrasalVerb.getParts().get(0);
            if (!sentence.getLemma().contains(verb))
                continue;

            String rest = String.join(" ", phrasalVerb.getParts());
            if (!sentence.getLemma().contains(rest))
                continue;

            for (int i = 0; i < sentence.getTerms().size(); i++) {
                if (sentence.getTerms().get(i).getLemma().equals(verb)) {
                    int verbIndex = i;
                    int start = -1;
                    int window = 4;

                    if (phrasalVerb.isInseparable())
                        window = 1;

                    for (int j = i; j < window+i && j < sentence.getTerms().size() - phrasalVerb.getParts().size()+1; j++) {
                        if (sentence.getTerms().get(j+1).getLemma().equals(verb)) {
                            start = -1;
                            break;
                        }
                        if (check(phrasalVerb, sentence, j))
                            start = j;
                    }
                    if (start !=-1) {
                        Pair<List<String>, Integer> listIntegerPair = candidates.get(verbIndex);
                        if (listIntegerPair==null || listIntegerPair.first().size() < phrasalVerb.getParts().size())
                            candidates.put(verbIndex, new Pair<>(phrasalVerb.getParts(), start));
                        //System.out.println(start + ", verb : " + phrasalVerb.getParts() + ", sentence : " + sentence.getLemma());
                    }
                }
            }

        }

        List<Term> newTerms = sentence.getTerms();
        for (Map.Entry<Integer, Pair<List<String>, Integer>> listPairEntry : candidates.entrySet()) {
            newTerms.get(listPairEntry.getKey()).setWord(String.join(" ", listPairEntry.getValue().first()));
            newTerms.get(listPairEntry.getKey()).setLemma(newTerms.get(listPairEntry.getKey()).getWord());
            newTerms.get(listPairEntry.getKey()).setPos("VB-PH");
            newTerms.get(listPairEntry.getKey()).setSynonyms(phrasalVerbs.get(newTerms.get(listPairEntry.getKey()).getWord()).getSynonyms());
        }

        for (Pair<List<String>, Integer> listPairEntry : candidates.values()) {
            for (int i = listPairEntry.second()+1; i < listPairEntry.first().size() + listPairEntry.second(); i++) {
                newTerms.get(i).setWord(null);
            }
        }

        List<Term> finalTerms = new ArrayList<>();
        for (Term newTerm : newTerms) {
            if (newTerm.getWord() != null)
                finalTerms.add(newTerm);
        }

        return new Sentence(sentence.getText(),finalTerms, sentence.getSentiment(),sentence.getNameEntities());
    }

    private static boolean check(PhrasalVerb phrasalVerb, Sentence sentence, int verbIndex) {
        for (int i = 1; i < phrasalVerb.getParts().size(); i++) {
            if (!sentence.getTerms().get(verbIndex+i).getWord().equals(phrasalVerb.getParts().get(i)))
                return false;
        }
        return true;
    }

    public static void main(String[] args) {

        List<Term> terms = new ArrayList<>();
        terms.add(new Term("I", "I", "", null, 0));
        terms.add(new Term("care", "care", "", null, 0));
        terms.add(new Term("for", "for", "", null, 0));
        terms.add(new Term("you", "you", "", null, 0));
        terms.add(new Term(".", ".", "", null, 0));
        Sentence sentence  = new Sentence("I care for you.", terms, (short)1, null);
        new PhrasalVerbs().replacePhrasalVerbs(sentence);


        terms = new ArrayList<>();
        terms.add(new Term("I", "I", "", null, 0));
        terms.add(new Term("cannot", "cannot", "", null, 0));
        terms.add(new Term("catch", "catch", "", null, 0));
        terms.add(new Term("up", "up", "", null, 0));
        terms.add(new Term("with", "with", "", null, 0));
        sentence  = new Sentence("I cannot catch up with", terms, (short)1, null);
        new PhrasalVerbs().replacePhrasalVerbs(sentence);

        terms = new ArrayList<>();
        terms.add(new Term("I", "I", "", null, 0));
        terms.add(new Term("cannot", "cannot", "", null, 0));
        terms.add(new Term("get", "get", "", null, 0));
        terms.add(new Term("it", "it", "", null, 0));
        terms.add(new Term("off", "off", "", null, 0));
        terms.add(new Term("with", "with", "", null, 0));
        sentence  = new Sentence("I cannot get it off with", terms, (short)1, null);
        new PhrasalVerbs().replacePhrasalVerbs(sentence);
    }
}
