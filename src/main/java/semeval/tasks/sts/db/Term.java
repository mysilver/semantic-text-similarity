package semeval.tasks.sts.db;

import java.util.Set;

public class Term implements Cloneable{
	private String word;
	private String lemma;
	private String pos;
	private Set<String> synonyms;
	private int sentiment;

	public Term(String word, String lemma, String pos, Set<String> synonyms, int sentiment) {
		this.word = word;
		this.lemma = lemma;
		this.pos = pos;
		this.synonyms = synonyms;
		this.sentiment = sentiment;
	}

	public String getWord() {
		return word;
	}

	public String getPos() {
		return pos;
	}

	public Set<String> getSynonyms() {
		return synonyms;
	}

	public String getLemma() {
		return lemma;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public void setLemma(String lemma) {
		this.lemma = lemma;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public void setSynonyms(Set<String> synonyms) {
		this.synonyms = synonyms;
	}

	public int getSentiment() {
		return sentiment;
	}

	public void setSentiment(int sentiment) {
		this.sentiment = sentiment;
	}

	@Override
	public String toString() {
		return "Term{" +
				"word='" + word + '\'' +
				", lemma='" + lemma + '\'' +
				", pos='" + pos + '\'' +
				", synonyms=" + synonyms +
				", sentiment=" + sentiment +
				'}';
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Term(this.getWord(), this.getLemma(), this.pos, this.synonyms, this.sentiment);
	}
}
