package ml.dataset.feature;

import java.util.ArrayList;
import java.util.List;

public class PossibleValues {

	public static List<String> booleanValues() {
		List<String> booleanValues = new ArrayList<>(2);
		booleanValues.add("true");
		booleanValues.add("false");
		return booleanValues;
	}

	public static List<String> intValues(int start, int end) {
		List<String> ret  = new ArrayList<>(end - start + 1);
		for (int i = start; i <= end; i++) {
			ret.add(String.valueOf(i));
		}
		return ret;
	}
}
