package ml.dataset.print.formatter;

import ml.dataset.DataSet;
import ml.dataset.feature.FeatureAttribute;
import ml.dataset.feature.FeatureVector;
import ml.dataset.FeatureTable;
import ml.dataset.feature.value.FeatureValue;

import java.util.List;

public class CsvDatasetFormatter implements DataSetFormatter {

    private static char comma = ',';
    private static char opps = '\'';
    private static char ln = '\n';

    @Override
    public String format(DataSet dataSet) {
        return toString(dataSet.getFeatureAttributes()) + ln + toString(dataSet.getTrainingData());
    }

    protected static String toString(FeatureTable featureTable) {
        StringBuilder stringBuilder= new StringBuilder();
        for (FeatureVector featureVector : featureTable.getFeatureVector()) {
            for (FeatureValue feature : featureVector.getFeatures()) {
                stringBuilder.append(feature.value()).append(comma);
            }
            stringBuilder.append(opps).append(featureVector.getClassName()).append(opps).append(ln);
        }
        return stringBuilder.toString();
    }

    protected static String toString(List<FeatureAttribute> featureAttributes) {
        if (featureAttributes == null)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        for (FeatureAttribute attr : featureAttributes) {
            stringBuilder.append(opps).append(attr.getName()).append(opps).append(comma);
        }

        return stringBuilder.substring(0, stringBuilder.length()-1);
    }
}
