package nlp.thesaurus;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mohammad-Ali on 12/05/2016.
 */
public class Thesaurus {

    Map<String, ThesaurusEntity> thesaurus;

    public Thesaurus(Map<String, ThesaurusEntity> thesaurus) {
        this.thesaurus = thesaurus;
    }

    public Thesaurus() {
        this.thesaurus = new HashMap<>();
    }

    public static Thesaurus load() {
        Thesaurus thesaurus = new Thesaurus();
        FileReader fileReader = null;
        try {
            String file = Thesaurus.class.getResource("/theasurs.txt").getPath();
            fileReader = new FileReader(file);
        } catch (Exception e) {
            String file = "theasurs.txt";
            try {
                fileReader = new FileReader(file);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }
        try (BufferedReader br = new BufferedReader(fileReader)) {
            String line;
            StringBuilder sb= new StringBuilder();
            while ((line = br.readLine())!=null)
                sb.append(line);
            Thesaurus thesaurus1 = ThesaurusBuilder.fromJson(sb.toString());

            int zero = 0;
            for (Map.Entry<String, ThesaurusEntity> pair : thesaurus1.thesaurus.entrySet()) {
               /* if (isAcceptable(pair.getValue()))
                    zero++;
                else*/
                    thesaurus.thesaurus.put(pair.getKey().toLowerCase(),pair.getValue());
            }
            System.out.println("thesaurus.size : " + (thesaurus.thesaurus.size() - zero));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return thesaurus;
    }

    private static boolean isAcceptable(ThesaurusEntity thesaurusEntity) {
        List<ThesaurusSense> collins = thesaurusEntity.getCollins();
        if (collins.size() == 0)
            collins = thesaurusEntity.getRogets();

        if (collins.size() == 0)
            collins = thesaurusEntity.getWordNet();

        return collins.get(0).getAntonyms() != null;
    }

    public Map<String, ThesaurusEntity> getPhrases() {
        Map<String, ThesaurusEntity> ret = new HashMap<>();
        thesaurus.entrySet().stream().filter(entry -> entry.getValue().getEntity().contains(" ")).forEach(entry -> ret.put(entry.getKey(), entry.getValue()));
        return ret;
    }

    public ThesaurusEntity getThesaurusEntity(String word) {
        return thesaurus.get(word.toLowerCase().trim());
    }
}
