package ml.dataset.feature.value;

public interface FeatureValue {
    public String value();
}
